package org.cocktail.agrhum.serveur.components;

import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlwebapp.common.test.integ.NavigationHelper;
import org.cocktail.fwkcktlwebapp.common.test.integ.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.PendingException;
import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Soit;

public class AuthentificationAgrhumSteps {
	
	private WebDriver getWebDriver() {
		return NavigationHelper.getNavigationHelper().getWebDriver();
	}
	
	@Alors("^elle arrive sur la page d'accueil avec le menu \"([^\"]*)\"$")
	public void elle_arrive_sur_la_page_d_accueil_avec_le_menu(String nomMenu) throws Throwable {
		String xpath = "//a[contains(text(),'" + nomMenu + "')]";
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

//		WebElement link = getWebDriver().findElement(By.xpath(xpath));
		assertTrue(getWebDriver().getPageSource().contains(nomMenu));
	}
}
