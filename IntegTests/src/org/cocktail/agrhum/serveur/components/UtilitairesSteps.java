package org.cocktail.agrhum.serveur.components;

import static org.cocktail.agrhum.serveur.components.UtilitairesSteps.getWebDriver;

import org.cocktail.fwkcktlwebapp.common.test.integ.NavigationHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UtilitairesSteps {

	public static WebDriver getWebDriver() {
		return NavigationHelper.getNavigationHelper().getWebDriver();
	}
	
	public static boolean isButtonSpanAInactif(String id) {
		return isElementTagName(id, "span");
	}
	
	public static boolean isButtonAActif(String id) {
		return isElementTagName(id, "a");
	}
	
	public static boolean isElementTagName(String id, String tagName) {
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 10);
		String path="//*[@id='" + id + "']";
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
		WebElement w=getWebDriver().findElement(By.xpath(path));
		return w.getTagName().equals(tagName);
	}
	
	public static boolean isElementPresent(String id) {
		return getWebDriver().findElements(By.id(id)).size() != 0;
	}
	
	public static void allerSurOnglet(String nomOnglet) {
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 10);
		String path="//*[@id='" + nomOnglet + "']";

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}
	
}
