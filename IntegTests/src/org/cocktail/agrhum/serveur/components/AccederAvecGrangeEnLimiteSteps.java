package org.cocktail.agrhum.serveur.components;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.cocktail.agrhum.serveur.components.UtilitairesSteps.*;

import org.cocktail.fwkcktlwebapp.common.test.integ.NavigationHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Lorsqu;
import cucumber.api.java.fr.Soit;


public class AccederAvecGrangeEnLimiteSteps {

	private static final String URL_ACCUEIL = System.getProperty("URL_ACCUEIL");
	
	private WebDriver getWebDriver() {
		return NavigationHelper.getNavigationHelper().getWebDriver();
	}
	
	@Soit("^\"([^\"]*)\" habilité à utiliser \"([^\"]*)\"$")
	public void habilite_a_utiliser(String identification, String application) {
		getWebDriver().navigate().to(URL_ACCUEIL);
    }

	@Lorsqu("^il se connecte avec le login \"([^\"]*)\" et le mot de passe\"([^\"]*)\"$")
	public void il_se_connecte_avec_le_login_et_le_mot_de_passe(String login, String motDePasse) throws Throwable {
		getWebDriver().findElement(By.id("LoginId")).sendKeys(login);
		getWebDriver().findElement(By.name("mot_de_passe")).sendKeys(motDePasse);
		getWebDriver().findElement(By.name("validerLogin")).click();
	}
	
	@Lorsqu("^qu'il arrive sur la page d'accueil avec le menu \"([^\"]*)\"$")
	public void qu_il_arrive_sur_la_page_d_accueil_avec_le_menu(String nomMenu) throws Throwable {
		Thread.sleep(2000);
		String xpath = "//a[contains(text(),'" + nomMenu + "')]";
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		assertTrue(getWebDriver().getPageSource().contains(nomMenu));
	}

	@Lorsqu("^qu'il saisie \"([^\"]*)\" dans la recherche$")
	public void qu_il_saisie_dans_la_recherche(String nomIndividu) throws Throwable {
		// On localise la zone du champs de recherche 
		// On renseigne avec "alexia"
		// On lance la recherche
		String xpath = "//html/body/div[4]/table/tbody/tr/td/div/div[1]/form/input";

		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		WebElement inputStatut = getWebDriver().findElement(By.xpath(xpath));
		assertTrue(inputStatut != null);
		inputStatut.sendKeys(nomIndividu);
		
	}

	@Lorsqu("^qu'il fait entrer$")
	public void qu_il_fait_entrer() throws Throwable {
		String xpath = "//html/body/div[5]/table/tbody/tr/td/div/div[1]/form";

		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a#validSrchBt")));
		
		WebElement boutonLoupe = getWebDriver().findElement(By.cssSelector("a#validSrchBt"));
		boutonLoupe.click();
	}

	@Lorsqu("^qu'il vérifie qu'elle est bien sur la fenêtre modale de recherche$")
	public void qu_il_vérifie_qu_elle_est_bien_sur_la_fenêtre_modale_de_recherche() throws Throwable {

		String xpath = "//html/body/div[@class='dialog']";
		
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
				
	}

	@Lorsqu("^qu'il clique sur le bouton \"([^\"]*)\"$")
	public void qu_il_clique_sur_le_bouton(String arg1) throws Throwable {
		// on cherche le bouton sélectionner dans la modal Windows
		// puis on prend le premier //input
		String xpath = "//html/body/div[@class='dialog']//a[@title='Sélectionner']";

		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		WebElement boutonSelectionner = getWebDriver().findElement(By.xpath(xpath));
		boutonSelectionner.click();
	}

	@Lorsqu("^qu'il vérifie qu'il est bien sur la page de \"([^\"]*)\"$")
	public void qu_il_vérifie_qu_il_est_bien_sur_la_page_de(String nomIndividu) throws Throwable {
		Thread.sleep(2000);
		assertTrue(getWebDriver().getPageSource().contains(nomIndividu));
	}

	@Lorsqu("^qu'il va sur l'onglet Comptes$")
	public void qu_il_va_sur_l_onglet_Comptes() throws Throwable {
		Thread.sleep(500);
		allerSurOnglet("AdminViewComptesTab");
	}
	
	@Alors("^le bouton supprimer est invisible$")
	public void le_bouton_supprimer_est_invisible() throws Throwable {
		Thread.sleep(500);
		
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 10);
		String path="//*[@id='SupprCompteBoutonId']";

		assertFalse(isElementPresent("SupprCompteBoutonId"));
		
	}
}
