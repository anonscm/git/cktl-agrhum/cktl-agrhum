package org.cocktail.agrhum.serveur.components;

import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlwebapp.common.test.integ.NavigationHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.PendingException;
import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Lorsqu;

public class ModifierDetailUtilisateurGroupeSteps {

	private WebDriver getWebDriver() {
		return NavigationHelper.getNavigationHelper().getWebDriver();
	}

	@Lorsqu("^(?:il|elle) sélectionne le groupe \"([^\"]*)\"$")
	public void elle_sélectionne_le_groupe(String nomGroupe) throws Throwable {
		NavigationHelper.getNavigationHelper().navigateToPageByLink(nomGroupe);
		assertTrue(getWebDriver().getPageSource().contains(nomGroupe));
	}

	@Lorsqu("^qu'(?:il|elle) clique sur le membre du groupe \"([^\"]*)\"$")
	public void qu_elle_clique_sur_le_membre_du_groupe(String nomMembre) throws Throwable {

		// on cherche la cellule du tableau qui contient le nom du membre et on remonte de 3 niveau pour avoir la ligne
		// puis on prend le premier //input
		String xpath = "//div[contains(text(),'" + nomMembre + "')]/../../..//input";
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		WebElement e = getWebDriver().findElement(By.xpath(xpath));
		assertTrue(e != null);
		e.click();
	}

	@Lorsqu("^qu'elle modifie le statut avec la valeur \"([^\"]*)\"$")
	public void qu_elle_modifie_le_statut_avec_la_valeur(String nouveauStatut) throws Throwable {
		String xpath = "//th[contains(text(),'Statut')]/..//input";

		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		WebElement inputStatut = getWebDriver().findElement(By.xpath(xpath));
		assertTrue(inputStatut != null);
		inputStatut.sendKeys(nouveauStatut);

		 NavigationHelper.getNavigationHelper().clickOnCktlAjaxActionButton("Enregistrer");
	}

	@Alors("^le statut de \"([^\"]*)\" a bien été modifié avec la valeur \"([^\"]*)\"$")
	public void le_statut_de_a_bien_été_modifié_avec_la_valeur(String nomMembre, String nouveauStatut) throws Throwable {
		String xpath = "//th[contains(text(),'Statut')]/..//input";

		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		assertTrue(nouveauStatut + " non trouvé dans la page", getWebDriver().getPageSource().contains(nouveauStatut));
	}

}
