package org.cocktail.agrhum.serveur.components;

import org.junit.Ignore;
import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

/**
 * Test fonctionnel : Vérification de l'accès par authentification à l'application
 */
@RunWith(Cucumber.class)
@Cucumber.Options(features = "classpath:authentificationAgrhum.feature", glue = { "org.cocktail.fwkcktlwebapp.common.test.integ",
"org.cocktail.agrhum.serveur.components" })

public class AuthentificationAgrhumTests {

}
