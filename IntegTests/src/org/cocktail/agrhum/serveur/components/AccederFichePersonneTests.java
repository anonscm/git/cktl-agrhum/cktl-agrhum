package org.cocktail.agrhum.serveur.components;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

/**
 * Test fonctionnel : Vérification de l'accès à la fiche d'un individu (ici Bertrand Vannier)
 */
@RunWith(Cucumber.class)
@Cucumber.Options(features = "classpath:accederFichePersonne.feature", glue = { "org.cocktail.fwkcktlwebapp.common.test.integ",
"org.cocktail.agrhum.serveur.components" })


public class AccederFichePersonneTests {

}
