package org.cocktail.agrhum.serveur.components;

import static org.junit.Assert.assertTrue;
import static org.cocktail.agrhum.serveur.components.UtilitairesSteps.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Lorsqu;

public class AccederFichePersonneSteps {
	

	@Lorsqu("^elle saisie \"([^\"]*)\" dans la recherche$")
	public void elle_saisie_dans_la_recherche(String nomIndividu) throws Throwable {
		// On localise la zone du champs de recherche 
		// On renseigne avec "Bertrand"
		// On lance la recherche
		String xpath = "//html/body/div[4]/table/tbody/tr/td/div/div[1]/form/input";

		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		WebElement inputStatut = getWebDriver().findElement(By.xpath(xpath));
		assertTrue(inputStatut != null);
		inputStatut.sendKeys(nomIndividu);
		
	}
	
	@Lorsqu("^qu'elle fait entrer$")
	public void qu_elle_fait_entrer() throws Throwable {
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a#validSrchBt")));
		
		WebElement boutonLoupe = getWebDriver().findElement(By.cssSelector("a#validSrchBt"));
		boutonLoupe.click();

	}

	@Lorsqu("^qu'elle vérifie qu'elle est bien sur la fenêtre modale de recherche$")
	public void qu_elle_vérifie_qu_elle_est_bien_sur_la_fenêtre_modale_de_recherche() throws Throwable {

		String xpath = "//html/body/div[@class='dialog']";
		
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
				
	}
	
	@Lorsqu("^qu'elle clique sur le bouton \"([^\"]*)\"$")
	public void qu_elle_clique_sur_le_bouton(String nouveauBouton) throws Throwable {
		// on cherche le bouton sélectionner dans la modal Windows
		// puis on prend le premier //input
		String xpath = "//html/body/div[@class='dialog']//a[@title='Sélectionner']";
	

		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		WebElement boutonSelectionner = getWebDriver().findElement(By.xpath(xpath));
		boutonSelectionner.click();
	}

	@Alors("^elle arrive sur la fiche de \"([^\"]*)\"$")
	public void elle_arrive_sur_la_fiche_de(String nomIndividu) throws Throwable {

		String xpath = "//html/body/div[3]/div[2]/form/ul[2]/li[1]/div[1]/div[2]/div[1]/div[2]/span";
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		assertTrue(getWebDriver().getPageSource().contains(nomIndividu));
	}

	@Lorsqu("^qu'elle vérifie qu'il est bien sur la page de \"([^\"]*)\"$")
	public void qu_elle_vérifie_qu_il_est_bien_sur_la_page_de(String nomIndividu) throws Throwable {
		Thread.sleep(2000);
		assertTrue(getWebDriver().getPageSource().contains(nomIndividu));
	}
	
	
	
	@Lorsqu("^qu'elle va sur l'onglet Groupes$")
	public void qu_elle_va_sur_l_onglet_Groupes() throws Throwable {
		Thread.sleep(2000);
		allerSurOnglet("AdminViewGrpTab");
	}
	

	@Lorsqu("^qu'elle sélectionne \"([^\"]*)\"$")
	public void qu_elle_sélectionne(String nomGroupe) throws Throwable {
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 10);
		String path="//html/body//optgroup/option[contains(text(),'" + nomGroupe + "')]";

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}

	@Alors("^la page contient le libelle \"([^\"]*)\" et le champ \"([^\"]*)\"$")
	public void la_page_contient_le_libelle_et_le_champ(String libelle, String champ) throws Throwable {
		Thread.sleep(4000);
		assertTrue(getWebDriver().getPageSource().contains(libelle));
		assertTrue(getWebDriver().getPageSource().contains(champ));
	}
	
	@Alors("^le bouton supprimer est inactif.$")
	public void le_bouton_supprimer_est_inactif() throws Throwable {
		Thread.sleep(500);
		assertTrue(isButtonSpanAInactif("SupprimerAffectationGroupeId"));
	}
	
	
	@Alors("^la page contient un libellé \"([^\"]*)\" et le champ \"([^\"]*)\"$")
	public void la_page_contient_un_libellé_et_le_champ(String libelle, String champ) throws Throwable {
		Thread.sleep(4000);
		assertTrue(getWebDriver().getPageSource().contains(libelle));
		assertTrue(getWebDriver().getPageSource().contains(champ));
	}

	@Alors("^le bouton supprimer est actif.$")
	public void le_bouton_supprimer_est_actif() throws Throwable {
		Thread.sleep(500);
		assertTrue(isButtonAActif("SupprimerAffectationGroupeId"));
		
	}
	
}
