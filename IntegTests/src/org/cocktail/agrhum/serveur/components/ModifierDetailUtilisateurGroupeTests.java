package org.cocktail.agrhum.serveur.components;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

/**
 * Test fonctionnel : modifier le détail d'un utilisateur depuis la gestion des groupes
 */
@RunWith(Cucumber.class)
@Cucumber.Options(features = "classpath:modifierDetailUtilisateurGroupe.feature", glue = { "org.cocktail.fwkcktlwebapp.common.test.integ",
        "org.cocktail.agrhum.serveur.components" })

public class ModifierDetailUtilisateurGroupeTests {

}
