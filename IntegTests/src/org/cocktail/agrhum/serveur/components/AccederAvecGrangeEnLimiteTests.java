package org.cocktail.agrhum.serveur.components;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

/**
 * Test fonctionnel : Accès avec Olivier Grange qui a des droits limités
 */
@RunWith(Cucumber.class)
@Cucumber.Options(features = "classpath:accederAvecGrangeEnLimite.feature", glue = { "org.cocktail.fwkcktlwebapp.common.test.integ",
"org.cocktail.agrhum.serveur.components" })


public class AccederAvecGrangeEnLimiteTests {

}
