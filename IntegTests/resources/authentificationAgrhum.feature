# language: fr
Fonctionnalité: Je modifie vérifie que l'on peut se connecter à l'application
  Afin de contrôler l'accès à l'application
  En tant qu'utilisateur de Agrhum
  Je dois me connecter avec le mot de passe et me retrouver sur le menu de la page d'accueil
  
Scénario: Connection avec Alexia
Soit "alexia" utilisateur connecté à "AGrhum" avec le mot de passe "motDePasse"
    Alors elle arrive sur la page d'accueil avec le menu "Structures Internes / Groupes"