# language: fr
Fonctionnalité: Je modifie le statut d'un utilisateur depuis la gestion des groupes
  Afin de modifier le statut d'un utilisateur
  En tant qu'utilisateur de Agrhum
  Je dois aller sur la page Gestion des groupe puis sélectionner le groupe puis sélectionner l'utilisateur puis modifier son statut puis enregistrer la modification
  
Scénario: 1 - Consultation par l'utilisateur Alexia de la fiche de Bertrand VANIER
Soit "alexia" utilisateur connecté à "AGrhum" avec le mot de passe "motDePasse"
    Lorsqu'elle arrive sur la page d'accueil avec le menu "Structures Internes / Groupes"  
    Et elle saisie "vani" dans la recherche
    Et qu'elle fait entrer
    Et qu'elle vérifie qu'elle est bien sur la fenêtre modale de recherche
    Et qu'elle clique sur le bouton "Sélectionner"
    Alors elle arrive sur la fiche de "VANIER Bertrand"
    
Scénario: 2 - Consultation par l'utilisateur Alexia des groupes auxquels est affecté Olivier GRANGE
Soit "alexia" utilisateur connecté à "AGrhum" avec le mot de passe "motDePasse"
    Lorsqu'elle arrive sur la page d'accueil avec le menu "Structures Internes / Groupes"  
    Et elle saisie "olivier" dans la recherche
    Et qu'elle fait entrer
    Et qu'elle vérifie qu'elle est bien sur la fenêtre modale de recherche
    Et qu'elle clique sur le bouton "Sélectionner"
    Et qu'elle vérifie qu'il est bien sur la page de "Grange Olivier"
    Et qu'elle va sur l'onglet Groupes
    Et qu'elle sélectionne "FOURNISSEURS VALIDES (INDIVIDUS)"
    Alors la page contient le libelle "Intitulé du groupe" et le champ "FOURNISSEURS VALIDES"
    Alors le bouton supprimer est inactif.
    
Scénario: 3 - Consultation par l'utilisateur Alexia des groupes auxquels est affecté Olivier GRANGE

    Et qu'elle sélectionne "DIVERS"
    Alors la page contient un libellé "Intitulé du groupe" et le champ "DIVERS"
    Alors le bouton supprimer est actif.