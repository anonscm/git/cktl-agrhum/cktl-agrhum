# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à des pages de l'application AGrhum
  En tant qu'utilisateur du PGI Cocktail
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe
  Puis je vais sur des pages ou des onglets de personne physique ou morale
  
  Scénario: 1 - Consultation par l'utilisateur Olivier Grange de l'onglet compte et qui n'a pas de droit de suppression (donc le bouton de suppression de compte invisible)
Soit "Olivier Grange" habilité à utiliser "AGrhum"
	Lorsqu'il se connecte avec le login "ogrange" et le mot de passe"Cocktail"
    Et qu'il arrive sur la page d'accueil avec le menu "Structures Internes / Groupes"  
    Et qu'il saisie "alexia" dans la recherche
    Et qu'il fait entrer
    Et qu'il vérifie qu'elle est bien sur la fenêtre modale de recherche
    Et qu'il clique sur le bouton "Sélectionner"
    Et qu'il vérifie qu'il est bien sur la page de "DUPONT Alexia"
    Et qu'il va sur l'onglet Comptes
    Alors le bouton supprimer est invisible
    