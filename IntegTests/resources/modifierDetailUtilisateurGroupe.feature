# language: fr
Fonctionnalité: Je modifie le statut d'un utilisateur depuis la gestion des groupes
  Afin de modifier le statut d'un utilisateur
  En tant qu'utilisateur de Agrhum
  Je dois aller sur la page Gestion des groupe puis sélectionner le groupe puis sélectionner l'utilisateur puis modifier son statut puis enregistrer la modification
  
Scénario: Modification du statut de l'utilisateur Alexia
Soit "alexia" utilisateur connecté à "AGrhum" avec le mot de passe "motDePasse"
    Lorsqu'elle arrive sur la page "Gestion des groupes" en cliquant sur le lien  "Structures Internes / Groupes"  
    Et elle sélectionne le groupe "HABILITATION AGRHUM"
    Et qu'elle clique sur le membre du groupe "DUPONT Alexia"
    Et qu'elle modifie le statut avec la valeur "statut A"
    Alors le statut de "DUPONT Alexia" a bien été modifié avec la valeur "statut A"
    
    