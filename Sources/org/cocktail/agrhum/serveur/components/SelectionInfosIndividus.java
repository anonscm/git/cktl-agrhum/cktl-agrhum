/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.apache.log4j.Logger;
import org.cocktail.agrhum.serveur.AgrhumParamManager;
import org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXS;

public class SelectionInfosIndividus extends MyWOComponent {

	private static final long serialVersionUID = 3607400336991144219L;
    private static final String EDITING_CONTEXT_BDG = "editingContext";
    private static final String STRUCTURE_BDG = "structure";
    
    public static final String LIBELLE_STRUCTURE_KP =  EORepartStructure.TO_PERSONNE_ELT_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY;
    public static final String DEFAULT_individuTableViewColonneKeys = "numero,nomPrenomAffichage,ville";
    public static final String BINDING_individuTableViewColonneKeys = "individuTableViewColonneKeys";
    
    static public Logger LOG = Logger.getLogger(SelectionInfosIndividus.class);
    
    private ERXDisplayGroup<IPersonne> selectedPersonnesDisplayGroup = new ERXDisplayGroup<IPersonne>();
    private ERXDisplayGroup<EORepartStructure> membresDisplayGroup = new ERXDisplayGroup<EORepartStructure>();
    private EOStructure structure;
    private NSArray<CktlAjaxTableViewColumn> colonnes;
    private EORepartStructure currentRepartStructure;
    
    private SelectionInfosIndividusCtrl ctrl;
    
    private Boolean selectedPersonneHasChanged = false;
    private NSArray<String> choixBrowser;
    private NSArray<String> listeSelectedInfos;
    private NSMutableArray<String> transfert;
    private NSMutableArray<String> destination;
    
    private NSArray<String> selectedChoixInfosDisponibles = null;
	private NSArray<String> selectedInfosChoisies = null;
	private String currentInfo;
    

    @SuppressWarnings("unchecked")
	public SelectionInfosIndividus(WOContext context) {
        super(context);
        
        ctrl = new SelectionInfosIndividusCtrl(this);
		setCtrl(ctrl);
        
        choixBrowser = initBrowser();
        listeSelectedInfos = new NSArray<String>();
        listeSelectedInfos = (NSArray<String>) NSArray.EmptyArray;
        
        transfert = new NSMutableArray<String>();
        destination = new NSMutableArray<String>();
        
        membresDisplayGroup.setDelegate(new MembresDisplayGroupDelegate());
    }
    
    public class MembresDisplayGroupDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedPersonneHasChanged(true);
		}
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public void appendToResponse(WOResponse response, WOContext context) {
        updateDisplayGroup();
        choixBrowser = initBrowser();
        listeSelectedInfos = new NSArray<String>();
        listeSelectedInfos = (NSArray<String>) NSArray.EmptyArray;
        super.appendToResponse(response, context);
    }
    
    /**
	 * @return the ctrl
	 */
	public SelectionInfosIndividusCtrl ctrl() {
		return ctrl;
	}

	/**
	 * @param ctrl
	 *            the ctrl to set
	 */
	public void setCtrl(SelectionInfosIndividusCtrl ctrl) {
		this.ctrl = ctrl;
	}
    
    
    public boolean hasMembres() {
        return getMembresDisplayGroup().allObjects().count() > 0;
    }
    
    public String browsersContId() {
        return "BrowserCont_" + uniqueDomId();
    }
    
    public String membresTbvId() {
        return "MemTbv_" + uniqueDomId();
    }
    
    public String membresContId() {
        return "MemCont_" + uniqueDomId();
    }
    
    public EOEditingContext editingContext() {
        return (EOEditingContext) valueForBinding(EDITING_CONTEXT_BDG);
    }
    
    public String selectionLabel() {
        if (getMembresDisplayGroup().selectedObjects().count() == 1) {
            return getMembresDisplayGroup().selectedObject().toPersonneElt().getNomPrenomAffichage();
        } else {
            return getMembresDisplayGroup().selectedObjects().count() + " personnes sélectionnées";
        }
    }
    
    public ERXDisplayGroup<IPersonne> selectedPersonnesDisplayGroup() {
        return selectedPersonnesDisplayGroup;
    }
    
    public ERXDisplayGroup<EORepartStructure> getMembresDisplayGroup() {
        return membresDisplayGroup;
    }
    
    public NSArray<CktlAjaxTableViewColumn> colonnes() {
        if (colonnes == null) {
            CktlAjaxTableViewColumn col0 = new CktlAjaxTableViewColumn();
            col0.setLibelle("Nom");
            CktlAjaxTableViewColumnAssociation asso0 = new CktlAjaxTableViewColumnAssociation(
                    "currentRepartStructure." + LIBELLE_STRUCTURE_KP, "Aucun libellé");
            col0.setAssociations(asso0);
            colonnes = new NSArray<CktlAjaxTableViewColumn>(col0);
        }
        return colonnes;
    }
    
    public EOStructure structure() {
        EOStructure structureTmp = (EOStructure) valueForBinding(STRUCTURE_BDG);
        if (!ERXEOControlUtilities.eoEquals(structure, structureTmp)) {
            structure = structureTmp;
        }
        return structure;
    }
    
    public boolean auMoinsUnMembreSelectionne() {
        return getMembresDisplayGroup().selectedObjects().count() >= 1;
    }
    
    public IPersonne personneSelectionnee() {
        return getMembresDisplayGroup().selectedObject().toPersonneElt();
    }
    
    public void updateDisplayGroup() {
        getMembresDisplayGroup().setSelectsFirstObjectAfterFetch(false);
//        getMembresDisplayGroup().setObjectArray(structure().toRepartStructuresElts(
//                null, ERXS.ascInsensitives(ViewMembresGroupe.LIBELLE_STRUCTURE_KP), false));
        /**
         * Le fetch n'est fait dans le Trombinoscope que pour les individus qui sont membres du groupe sélectionné.
         * Les sous-structures ont été pour l'instant volontairement éliminées du fetch.
         */
        getMembresDisplayGroup().setObjectArray(structure().toRepartStructuresElts(null, ERXS.ascInsensitives(ViewMembresGroupe.LIBELLE_STRUCTURE_KP), false));
        getMembresDisplayGroup().setSelectedObjects(NSArray.EmptyArray);
    }
    
    public String membresDetailId() {
        return "MemDetails_" + uniqueDomId();
    }
    
	public Boolean getSelectedPersonneHasChanged() {
		return selectedPersonneHasChanged;
	}

	public void setSelectedPersonneHasChanged(Boolean selectedPersonneHasChanged) {
		this.selectedPersonneHasChanged = selectedPersonneHasChanged;
	}

	public EORepartStructure getCurrentRepartStructure() {
        return currentRepartStructure;
    }
    
    public void setCurrentRepartStructure(EORepartStructure currentRepartStructure) {
        this.currentRepartStructure = currentRepartStructure;
    }
   
    
    
    public NSArray<String> getChoixBrowser() {
		return choixBrowser;
	}

	public void setChoixBrowser(NSArray<String> choixBrowser) {
		this.choixBrowser = choixBrowser;
	}

	public NSArray<String> listeBrowser(){
    	if (choixBrowser == null) {
    		choixBrowser = initBrowser();
    	}
    	return choixBrowser;
    }
	
    public NSArray<String> initBrowser(){
    	NSMutableArray<String> initChoixBrowser = new NSMutableArray<String>();

    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_IDENTITE_CIV_NOM_PRENOM); // "Identité (Civ. + Nom + Prénom)"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_IDENTITE_CIV_PRENOM_NOM); //"Identité bis (Civ. + Prénom + Nom)"
    	if (isAccesToBirthDate()) {
    		initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_BIRTH_DATE); //"Date de naissance"
    	}
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_FONCTION_QUALITE); //"Fonction/Qualité"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_ACTIVITE); //"Activité"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_LOGIN); //"Login"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_EMAIL); //"Email"
    	if (isPasswordVisible()) {
    		initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_PASSWORD); //"Mot de passe"
    	}
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_EMAIL_PERSO); //"Email (Perso)"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_ADRESSE_BP_PRO); //"Adresse (Pro) + BP"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_CP_VILLE_PRO); //"CP + Ville (Pro)"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_TEL_PRO); //"Tél professionnel"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_FAX_PRO); //"Fax professionnel"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_ADRESSE_BP_PERSO); //"Adresse (Perso) + BP"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_CP_VILLE_PERSO); //"CP + Ville (Perso)"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_TEL_PERSO); //"Tél personnel"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_SERVICE); //"Service"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_POLE); //"Pôle"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_CORPS); //"Corps"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_FONCTION_INDIVIDU); //"Fonction Individu"
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_ENTREPRISE); //"Entreprise"
    	
    	initChoixBrowser.add(SelectionInfosIndividusCtrl.INFO_ADRESSE_FACTURATION); //"adresse de facturation"
    	
    	return initChoixBrowser;
    }
    
    public WOActionResults ajouterInfosChoisies() {
    	int index;
		if (selectedChoixInfosDisponibles != null) {
			if (getListeSelectedInfos() != null) {
				transfert = getListeSelectedInfos().mutableClone();
			}
			for (String info : selectedChoixInfosDisponibles) {
				if (!selectedInfosChoisies.contains(info)) {
					index = listeBrowser().indexOf(info);
					listeBrowser().remove(index);
					transfert.add(info);
				}
				
			}
			setListeSelectedInfos(transfert.immutableClone());
		}
		setSelectedChoixInfosDisponibles(null);
		transfert.removeAllObjects();
		return doNothing();
	}
    
    @SuppressWarnings("unchecked")
	public WOActionResults supprimerInfosChoisies() {
    	int index;
    	transfert = getListeSelectedInfos().mutableClone();
    	destination = listeBrowser().mutableClone();
		if (selectedInfosChoisies != null) {
			for (String info : selectedInfosChoisies) {
					if (!selectedChoixInfosDisponibles.contains(info)) {
						transfert.remove(info);
					}
			}
			setListeSelectedInfos(transfert.immutableClone());
			choixBrowser = initBrowser();
			for (String info : listeSelectedInfos) {
				destination.remove(info);
				index = listeBrowser().indexOf(info);
				listeBrowser().remove(index);
			}
		setSelectedChoixInfosDisponibles((NSArray<String>) NSArray.EmptyArray);
		}
		
		transfert.removeAllObjects();
		destination.removeAllObjects();
		
		return doNothing();
	}
    
	public String getCurrentInfo() {
		return currentInfo;
	}

	public void setCurrentInfo(String currentInfo) {
		this.currentInfo = currentInfo;
	}
	
	public NSArray<String> getListeSelectedInfos() {
		return listeSelectedInfos;
	}

	public void setListeSelectedInfos(NSArray<String> listeSelectedInfos) {
		this.listeSelectedInfos = listeSelectedInfos;
	}

	public NSArray<String> getSelectedChoixInfosDisponibles() {
		return selectedChoixInfosDisponibles;
	}

	public void setSelectedChoixInfosDisponibles(
			NSArray<String> selectedChoixInfosDisponibles) {
		this.selectedChoixInfosDisponibles = selectedChoixInfosDisponibles;
	}

	public NSArray<String> getSelectedInfosChoisies() {
		return selectedInfosChoisies;
	}

	public void setSelectedInfosChoisies(NSArray<String> selectedInfosChoisies) {
		this.selectedInfosChoisies = selectedInfosChoisies;
	}


	public WOActionResults onClose() {
		getMembresDisplayGroup().setSelectedObjects(NSArray.EmptyArray);
		return null;
	}
	
	public WOActionResults openExportPreview(){
		return null;
	}
	
	public NSArray<EORepartStructure> listeSelection(){
		return getMembresDisplayGroup().selectedObjects();
	}
	
	public String individuTableViewColonneKeys() {
		return stringValueForBinding(BINDING_individuTableViewColonneKeys, DEFAULT_individuTableViewColonneKeys);
	}
	

	@SuppressWarnings("unchecked")
	public WOActionResults onRecommencer() {
		getMembresDisplayGroup().clearSelection();
		choixBrowser = initBrowser();
        listeSelectedInfos = new NSArray<String>();
        listeSelectedInfos = (NSArray<String>) NSArray.EmptyArray;
        // Possible que ce ne soit pas nécessaire ci-dessous
        selectedChoixInfosDisponibles = null;
        selectedInfosChoisies = null;
		return null;
	}
	
	public WOActionResults openMultipleChoice(){
		InfosIndividuExportExcel infosIndividuExportExcel = (InfosIndividuExportExcel) pageWithName(InfosIndividuExportExcel.class.getName());
		infosIndividuExportExcel.setCtrl(ctrl);
		infosIndividuExportExcel.setListeSelection(listeSelection());
		infosIndividuExportExcel.setSelectedInfosChoisies(getListeSelectedInfos());
		return infosIndividuExportExcel;
	}
	
	public WOActionResults quitter() {
		editingContext().revert();
		ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
		WOComponent accueil = pageWithName(Accueil.class.getName());
		redirect.setComponent(accueil);
		return redirect;
	}
	
	public WOActionResults reload(){
		return null;
	}
	
	
	/**
	 * Règles qui activent et désactivent un bouton d'export
	 */
	
	// 5 choix max pour les étiquettes
	public boolean isEtiquetteDisabled(){
		if ((listeSelectedInfos.count() > 0) && (listeSelectedInfos.count() < 6)) {
			return false;
		}
		return true;
	}
	
	// 3 choix pour les badges vierges
	public boolean isBadgeViergeDisabled(){
		if ((listeSelectedInfos.count() > 0) && (listeSelectedInfos.count() < 4)) {
			return false;
		}
		return true;
	}
	
	// 2 choix pour les badges pré-remplis
	public boolean isBadgePreImpDisabled(){
		if ((listeSelectedInfos.count() > 0) && (listeSelectedInfos.count() < 3)) {
			return false;
		}
		return true;
	}
	
	// Au moins une sélection pour les exports au format Excel
	public boolean isExportExcelDisabled(){
		if (listeSelectedInfos.count() > 0) {
			return false;
		}
		return true;
	}
	

	public boolean isPasswordVisible() {
		if (application().config().booleanForKey(AgrhumParamManager.AGRHUM_EXPORT_PASSWORD)) {
			return true;
		}
		return false;
	}
	
	public boolean isAccesToBirthDate() {
		if (application().config().booleanForKey(AgrhumParamManager.AGRHUM_EXPORT_BIRTHDATE) && applicationUser().hasDroitRhTrombi()) {
			return true;
		}
		return false;
	}
}