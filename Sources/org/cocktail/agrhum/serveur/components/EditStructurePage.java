/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.eoaccess.EOGeneralAdaptorException;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXRedirect;

public class EditStructurePage extends MyWOComponent {
	private static final long serialVersionUID = 9215416092233119361L;
	private EOStructure structure;
	private boolean resetTabs;

	public EditStructurePage(WOContext context) {
        super(context);
    }

	public boolean isResetTabs() {
		return resetTabs;
	}

	public void setResetTabs(boolean resetTabs) {
		this.resetTabs = resetTabs;
	}
	
	public EOStructure getStructure() {
		return structure;
	}

	public void setStructure(EOStructure structure) {
		this.structure = structure;
	}
	
	public WOActionResults annuler() {
		ERXRedirect redirect = (ERXRedirect)pageWithName(ERXRedirect.class.getName());
        WOComponent accueil = pageWithName(Accueil.class.getName());
        redirect.setComponent(accueil);
        return redirect;
	}

	public WOActionResults terminer() {
		save();
		return null;
	}
	
	private void save() {
		try {
			edc().saveChanges();
			session().addSimpleInfoMessage(session().localizer().localizedStringForKey("confirmation"), null);
		} catch (NSValidation.ValidationException e) {
			logger().info(e.getMessage(), e);
			session().addSimpleErrorMessage(e.getLocalizedMessage(), e);
			context().response().setStatus(500);
		} catch (EOGeneralAdaptorException e) {
		    edc().revert();
			logger().warn(e.getMessage(), e);
			context().response().setStatus(500);
			session().addSimpleErrorMessage("Une erreur est survenue lors de l'enregistrement en base", e);
		} finally {
			session().reactivationTrigger();
		}
	}

	public String getEditStructureContainerId() {
		return getComponentId()+"_EditStructureContainer";
	}
	
}