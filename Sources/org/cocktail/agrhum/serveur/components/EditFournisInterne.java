/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForFournisseurSpec;
import org.cocktail.fwkcktlpersonne.common.eospecificites.FournisseurInterneServices;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSForwardException;

public class EditFournisInterne extends MyWOComponent {
	private static final long serialVersionUID = 2717708417647369975L;

	private static final Logger LOG = Logger.getLogger(EditFournisInterne.class);

	private static final String EDITING_CONTEXT_BDG = "editingContext";
	private static final String STRUCTURE_BDG = "structure";
	private static final String WANT_RESET = "wantReset";

	private FournisseurInterneServices fournisseurInterneServices = new FournisseurInterneServices();

	public EditFournisInterne(WOContext context) {
		super(context);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	@Override
	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
		if (wantReset()) {
			setWantReset(false);
		}
		super.appendToResponse(woresponse, wocontext);
	}

	public EOEditingContext editingContext() {
		return (EOEditingContext) valueForBinding(EDITING_CONTEXT_BDG);
	}

	private Boolean wantReset() {
		return hasBinding(WANT_RESET) && (Boolean) valueForBinding(WANT_RESET);
	}

	private void setWantReset(Boolean value) {
		setValueForBinding(value, WANT_RESET);
	}

	public void setStructureOB(EOStructure structureOB) {
		setValueForBinding(structureOB, STRUCTURE_BDG);
	}

	public EOStructure structureOB() {
		return (EOStructure) valueForBinding(STRUCTURE_BDG);
	}

	public boolean isFournisseurInterne() {
		return fournisseurInterneServices.isStructureFournisseurInterne(structureOB());
	}

	/**
	 * @return déclare la structure en tant que fournisseur interne
	 */
	public WOActionResults declareFournisseurInterne() {
		try {
			fournisseurInterneServices.declareFournisseurInterne(structureOB(), session().applicationUser());
			editingContext().saveChanges();
			session().addSimpleInfoMessage(localizer().localizedStringForKey("confirmation"), null);			
		} catch (NSForwardException e) {
			editingContext().revert();
			LOG.error(e.getMessage(), e);
			context().response().setStatus(500);
			if (e.originalException() != null) {
				session().addSimpleErrorMessage(e.originalException().getMessage(), e.originalException());
			}
		} catch (Exception e1) {
			editingContext().revert();
			LOG.error(e1.getMessage(), e1);
			context().response().setStatus(500);
			session().addSimpleErrorMessage(e1.getMessage(), e1);			
		}
		return null;
	}

	public Boolean allowEditNoInsee() {
//		if (structureOB() instanceof EOStructure){
//			return false;
//		}
//		return true;
		return false;
	}

	public Boolean showNoInsee() {
		return myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PARAM_FOURNIS_FORM_SHOWINSEE);
	}
}