/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import static org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl.isChaineOui;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;
import er.extensions.foundation.ERXStringUtilities;

public class TrombiPreview extends MyWOComponent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 252627282930L;
	private EORepartStructure unMembre;
	public static final String BDG_LIST_SELECTION = "listeSelection";
	public static final String BDG_STRUCTURE = "structure";
	
	static public Logger LOG = Logger.getLogger(TrombiPreview.class);
	
	private ReporterAjaxProgress reporterProgress;
	private String reportFilename;
	private CktlAbstractReporter reporter;
	
	private String imWidth;
	private String imHeight;
	
    public TrombiPreview(WOContext context) {
        super(context);
        
    }
    
    public EOEditingContext editingContext(){
    	return getStructure().editingContext();
    }
    
    public NSArray<EORepartStructure> getMembresDuGroupe(){
    	
    	if (hasBinding(BDG_LIST_SELECTION)) {
    		NSArray<EORepartStructure> listeSelection = (NSArray<EORepartStructure>) valueForBinding(BDG_LIST_SELECTION);
    		return listeSelection;
    	}
    	return null;
    }
    
    public boolean hasSelection() {
    	if (getMembresDuGroupe().count() > 0) {
    		return true;
    	}
    	return false;
    }
    
	public String getImHeight() {
		if (imHeight == null) {
			 imHeight = EOGrhumParametres.parametrePourCle(editingContext(), FwkCktlPersonneGuiAjaxParamManager.GRHUM_PHOTO_HEIGHT);
		}
		return imHeight;
	}

	public void setImHeight(String imHeight) {
		this.imHeight = imHeight;
	}
	
	public String getImWidth() {
		if (imWidth == null) {
			 imWidth = EOGrhumParametres.parametrePourCle(editingContext(), FwkCktlPersonneGuiAjaxParamManager.GRHUM_PHOTO_HEIGHT);
			 imWidth = myApp().config().stringForKey("IMAGE_WIDTH");
		}
		return imWidth;
	}

	public void setImWidth(String imWidth) {
		this.imWidth = imWidth;
	}

	public EOStructure getStructure() {
		return (EOStructure) valueForBinding(BDG_STRUCTURE);
	}
	
	public void setStructure(EOStructure structure) {
		setValueForBinding(structure, TrombiPreview.BDG_STRUCTURE);
	}
	
	public EORepartStructure getUnMembre() {
		return unMembre;
	}

	public void setUnMembre(EORepartStructure unMembre) {
		this.unMembre = unMembre;
	}
	
	public EOIndividu individu(){
		return (EOIndividu) getUnMembre().toPersonneElt();
	}
    
	public boolean hasStructure() {
		if (getStructure() != null) {
			return true;
		}
		return false;
	}
	
	public String textAlternatif() {
		return ("Logo de " + EOGrhumParametres.parametrePourCle(editingContext(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY));
	}
	
	/**
     * @return true si une photo est affichable
     */
    public boolean hasPhoto() {
    	
    	if (individu().toPhoto() == null || individu().toPhoto().datasPhoto() == null) {
    		return false;
    	}
    	
    	if(applicationUser().hasDroitRhTrombi()) {
			return true;
		}
    	// On teste si la photo n'est pas privée
    	if (MyStringCtrl.isChaineOui(individu().indPhoto())) {
    		return true;
    	}
        return false;
    }
    
    
    public CktlAbstractReporter getReporter() {
        return reporter;
    }

	public CktlAbstractReporterAjaxProgress getReporterProgress() {
        return reporterProgress;
    }
	
	public String getReportFilename() {
        return reportFilename;
    }
	
	public void setReportFilename(String reportFilename) {
        this.reportFilename = reportFilename;
    }
    
	
	public static class ReporterAjaxProgress extends CktlAbstractReporterAjaxProgress implements org.cocktail.reporting.server.jrxml.IJrxmlReportListener {

        public ReporterAjaxProgress(int maximum) {
            super(maximum);
        }

	}
	
	public WOActionResults onPrint() {
		reporterProgress = new ReporterAjaxProgress(100);

		reportFilename = "ImpressionTrombiDuGroupeSelectionne.pdf";
		reporter = reporter(getStructure(), reporterProgress);

		return doNothing();
	}
    
	public CktlAbstractReporter reporter(EOStructure uniteRecherche, IJrxmlReportListener listener) {

		String jasperFileName = "report/Trombi/Trombi.jasper";

		String recordPath = "/groupe/membre";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			StringWriter xmlString = creerXml(uniteRecherche);
			InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression de la sélection des membres d'un groupe", xmlFileStream, recordPath, pathForJasper(jasperFileName), parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);
		} catch (Throwable e) {
			LOG.error("Une erreur s'est produite durant l'edition du trombinoscope d'un groupe", e);
		}
		return jr;

	}

	protected String pathForJasper(String reportPath) {
        ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
        URL url = rsm.pathURLForResourceNamed(reportPath, "app", null);
        return url.getFile();
    }
	
	public StringWriter creerXml(EOStructure groupe) throws Exception {

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		w.startDocument();
		w.startElement("groupe");

		w.writeElement("libelle", ERXStringUtilities.stringIsNullOrEmpty(groupe.llStructure()) ? groupe.lcStructure() : groupe.llStructure());

		NSArray<EORepartStructure> elementsSelectionnes = getMembresDuGroupe();
		
		Integer total = 0;
		
		for (EORepartStructure membre : elementsSelectionnes) {
			
			EOIndividu individuMembre = (EOIndividu) membre.toPersonneElt();
			
			w.startElement("membre");
			
			String photo_encodee = null;
			if (individuMembre.toPhoto() == null || individuMembre.toPhoto().datasPhoto() == null) {
				photo_encodee = Base64.encode(application().resourceManager().bytesForResourceNamed("images/nophoto.jpg", "FwkCktlThemes", null));
			} else if (isAutoriseAVoirPhoto(individuMembre)) {
				photo_encodee = Base64.encode(individuMembre.toPhoto().datasPhoto().bytes());
			} else {
				photo_encodee = Base64.encode(application().resourceManager().bytesForResourceNamed("images/nophoto.jpg", "FwkCktlThemes", null));
			}
			
			w.writeElement("photo", photo_encodee);
			w.writeElement("nom", individuMembre.nomAffichage());
			w.writeElement("prenom", individuMembre.prenomAffichage());
			
			w.endElement();
			
			total += 1;
			
		}

		w.writeElement("total", "" + total + " personne" + (total > 1 ? "s" : ""));

		w.endElement();
		w.endDocument();
		w.close();

		return sw;

	}
	
	public boolean isAutoriseAVoirPhoto(EOIndividu individuMembre) {
		if (applicationUser().hasDroitRhTrombi()) {
			return true;
		}
		if (isChaineOui(individuMembre.indPhoto())) {
			return true;
		}
		return false;
	}
    
	
	public boolean isMembreIndividu() {
		if (getUnMembre() == null || getUnMembre().toPersonneElt() == null) {
			return false;
		}
		
		return getUnMembre().toPersonneElt().isIndividu();
	}
}