package org.cocktail.agrhum.serveur.components.controlers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.agrhum.serveur.AgrhumParamManager;
import org.cocktail.agrhum.serveur.components.MyWOComponent;
import org.cocktail.agrhum.serveur.components.SelectionInfosIndividus;
import org.cocktail.agrhum.serveur.components.TrombiPreview.ReporterAjaxProgress;
import org.cocktail.agrhum.serveur.export.ExportEngine;
import org.cocktail.agrhum.serveur.export.ExportIdentite;
import org.cocktail.agrhum.serveur.export.ExportIdentiteBis;
import org.cocktail.agrhum.serveur.export.IExportInformations;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriere;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.server.util.CompteService;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.google.inject.Inject;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;

public class SelectionInfosIndividusCtrl {

	// Constantes d'accès aux informations de la personne
	// **************************************************
	public static final String INFO_IDENTITE_CIV_NOM_PRENOM = "Identité (Civ. + Nom + Prénom) *";
	public static final String INFO_IDENTITE_CIV_PRENOM_NOM = "Identité bis (Civ. + Prénom + Nom) *";
	public static final String INFO_ACTIVITE = "Activité *";
	public static final String INFO_FONCTION_QUALITE = "Fonction/Qualité";
	public static final String INFO_LOGIN = "Login *";
	public static final String INFO_EMAIL = "Email *";
	public static final String INFO_EMAIL_PERSO = "Email (Perso)";
	public static final String INFO_ADRESSE_BP_PRO = "Adresse (Pro) + BP *";
	public static final String INFO_CP_VILLE_PRO = "CP + Ville (Pro) *";
	public static final String INFO_TEL_PRO = "Tél professionnel *";
	public static final String INFO_FAX_PRO = "Fax professionnel *";
	public static final String INFO_ADRESSE_BP_PERSO = "Adresse (Perso) + BP";
	public static final String INFO_CP_VILLE_PERSO = "CP + Ville (Perso)";
	public static final String INFO_TEL_PERSO = "Tél personnel";
	public static final String INFO_POLE = "Pôle";
	public static final String INFO_POLE_LIBELLE_COMPOSANTE = "Composante";
	public static final String INFO_FONCTION_INDIVIDU = "Fonction Individu";
	public static final String INFO_ENTREPRISE = "Entreprise";
	public static final String INFO_SERVICE = "Service (Affectation RH lié au contrat)";
	public static final String INFO_CORPS = "Corps";
	public static final String INFO_CORPS_CODE = "codeCorps";
	public static final String INFO_CORPS_LL = "llCorps";
	public static final String INFO_CORPS_LC = "lcCorps";
	public static final String INFO_ADRESSE_FACTURATION = "Adresse de facturation";

	public static final String INFO_PASSWORD = "Mot de passe";
	public static final String INFO_BIRTH_DATE = "Date de naissance";
	
	private ReporterAjaxProgress reporterProgress;
	private String reportFilename;
	private CktlAbstractReporter reporter;
	
	public SelectionInfosIndividus wocomponent;
	public MyWOComponent mywocomponent;
	public EOEditingContext edc;
	
    
    
	public SelectionInfosIndividusCtrl(SelectionInfosIndividus aWOComponent) {
		wocomponent = aWOComponent;
		edc = wocomponent.session().defaultEditingContext();
    }
	
	public EOEditingContext editingContext() {
        return edc;
    }
	

	
	/* ************************************************************************************************************************************************************* */
	public ReporterAjaxProgress getReporterProgress() {
		return reporterProgress;
	}

	public void setReporterProgress(ReporterAjaxProgress reporterProgress) {
		this.reporterProgress = reporterProgress;
	}

	public String getReportFilename() {
		return reportFilename;
	}

	public void setReportFilename(String reportFilename) {
		this.reportFilename = reportFilename;
	}

	public CktlAbstractReporter getReporter() {
		return reporter;
	}

	public void setReporter(CktlAbstractReporter reporter) {
		this.reporter = reporter;
	}
	
	public WOActionResults doNothing() {
		return null;
	}
	

	
	
	public StringWriter creerXml(EOStructure groupe) throws Exception {
		ExportEngine engine = new ExportEngine();
		engine.setEditingContext(editingContext());
		return engine.creerXml(groupe,wocomponent.listeSelection(),wocomponent.getListeSelectedInfos());
	}


	
	protected String pathForJasper(String reportPath) {
        ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
        URL url = rsm.pathURLForResourceNamed(reportPath, "app", null);
        return url.getFile();
    }
	
	@SuppressWarnings("static-access")
	public CktlAbstractReporter reporterEtiq28SM(EOStructure groupe, IJrxmlReportListener listener) {
		
		String jasperFileName = "report/Export/Etiquette28SM.jasper";
		
		String recordPath = "/groupe/membre";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			StringWriter xmlString = creerXml(groupe);
			InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression des étiquettes 2X8 sans marge de la sélection", xmlFileStream, recordPath, pathForJasper(jasperFileName),
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);
		} catch (Throwable e) {
			wocomponent.LOG.error("Une erreur s'est produite durant l'édition des étiquettes 2X8 sans marge d'un groupe", e);
		}
		return jr;
	}
	
	public WOActionResults onPrintEtiq28SM() {
		reporterProgress = new ReporterAjaxProgress(100);
		
		reportFilename = "ImpressionEtiquette28SMDuGroupeSelectionne" + DateCtrl.currentDateHeureString() + ".pdf";
		reporter = reporterEtiq28SM(wocomponent.structure(), reporterProgress);
		
		return doNothing();
	}
	
	@SuppressWarnings("static-access")
	public CktlAbstractReporter reporterEtiq38SM(EOStructure groupe, IJrxmlReportListener listener) {
		
		String jasperFileName = "report/Export/Etiquette38SM.jasper";
		
		String recordPath = "/groupe/membre";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			StringWriter xmlString = creerXml(groupe);
			InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression des étiquettes 3X8 sans marge de la sélection", xmlFileStream, recordPath, pathForJasper(jasperFileName),
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);
		} catch (Throwable e) {
			wocomponent.LOG.error("Une erreur s'est produite durant l'édition des étiquettes 3X8 sans marge d'un groupe", e);
		}
		return jr;
	}
	
	public WOActionResults onPrintEtiq38SM() {
		reporterProgress = new ReporterAjaxProgress(100);
		
		reportFilename = "ImpressionEtiquette38SMDuGroupeSelectionne" + DateCtrl.currentDateHeureString() + ".pdf";
		reporter = reporterEtiq38SM(wocomponent.structure(), reporterProgress);
		
		return doNothing();
	}
	
	@SuppressWarnings("static-access")
	public CktlAbstractReporter reporterEtiq28MHB(EOStructure groupe, IJrxmlReportListener listener) {
		
		String jasperFileName = "report/Export/Etiquette28MHB.jasper";
		
		String recordPath = "/groupe/membre";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			StringWriter xmlString = creerXml(groupe);
			InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression des étiquettes 2X8 avec marges en haut et en bas de la sélection", xmlFileStream, recordPath, pathForJasper(jasperFileName),
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);
		} catch (Throwable e) {
			wocomponent.LOG.error("Une erreur s'est produite durant l'édition des étiquettes 2X8 avec marges Haut et Bas d'un groupe", e);
		}
		return jr;
	}
	
	public WOActionResults onPrintEtiq28MHB() {
		reporterProgress = new ReporterAjaxProgress(100);
		
		reportFilename = "ImpressionEtiquette28MHBDuGroupeSelectionne" + DateCtrl.currentDateHeureString() + ".pdf";
		reporter = reporterEtiq28MHB(wocomponent.structure(), reporterProgress);
		
		return doNothing();
	}
	
	@SuppressWarnings("static-access")
	public CktlAbstractReporter reporterEtiq38MHB(EOStructure groupe, IJrxmlReportListener listener) {
		
		String jasperFileName = "report/Export/Etiquette38MHB.jasper";
		
		String recordPath = "/groupe/membre";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			StringWriter xmlString = creerXml(groupe);
			InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression des étiquettes 3X8 avec marges en haut et en bas de la sélection", xmlFileStream, recordPath, pathForJasper(jasperFileName),
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);
		} catch (Throwable e) {
			wocomponent.LOG.error("Une erreur s'est produite durant l'édition des étiquettes 3X8 avec marges Haut et Bas d'un groupe", e);
		}
		return jr;
	}
	
	public WOActionResults onPrintEtiq38MHB() {
		reporterProgress = new ReporterAjaxProgress(100);
		
		reportFilename = "ImpressionEtiquette38MHBDuGroupeSelectionne" + DateCtrl.currentDateHeureString() + ".pdf";
		reporter = reporterEtiq38MHB(wocomponent.structure(), reporterProgress);
		
		return doNothing();
	}
	
	@SuppressWarnings("static-access")
	public CktlAbstractReporter reporterBagdeVierge(EOStructure groupe, IJrxmlReportListener listener) {
		
		String jasperFileName = "report/Export/BadgeVierge.jasper";
		
		String recordPath = "/groupe/membre";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			StringWriter xmlString = creerXml(groupe);
			InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression des bagdes vierges de la sélection", xmlFileStream, recordPath, pathForJasper(jasperFileName),
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);
		} catch (Throwable e) {
			wocomponent.LOG.error("Une erreur s'est produite durant l'édition des badges vierges d'un groupe", e);
		}
		return jr;
	}
	
	public WOActionResults onPrintBagdeVierge() {
		reporterProgress = new ReporterAjaxProgress(100);
		
		reportFilename = "ImpressionBagdeViergeDuGroupeSelectionne" + DateCtrl.currentDateHeureString() + ".pdf";
		reporter = reporterBagdeVierge(wocomponent.structure(), reporterProgress);
		
		return doNothing();
	}
	
	@SuppressWarnings("static-access")
	public CktlAbstractReporter reporterBadgePreImp(EOStructure groupe, IJrxmlReportListener listener) {
		
		String jasperFileName = "report/Export/BadgePreImp.jasper";
		
		String recordPath = "/groupe/membre";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			StringWriter xmlString = creerXml(groupe);
			InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression des bagdes pré-imprimés de la sélection", xmlFileStream, recordPath, pathForJasper(jasperFileName),
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);
		} catch (Throwable e) {
			wocomponent.LOG.error("Une erreur s'est produite durant l'édition des badges pré-imprimés d'un groupe", e);
		}
		return jr;
	}
	
	public WOActionResults onPrintBadgePreImp() {
		reporterProgress = new ReporterAjaxProgress(100);
		
		reportFilename = "ImpressionBadgePreImpDuGroupeSelectionne" + DateCtrl.currentDateHeureString() + ".pdf";
		reporter = reporterBadgePreImp(wocomponent.structure(), reporterProgress);
		
		return doNothing();
	}
	
	
}
