/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEC;

public class TrombiPage extends MyWOComponent {
	
	private static final long serialVersionUID = 3772803841626364183L;
	
	private EOStructure structure;
	private NSDictionary<String, EOQualifier> filters;
	private String sectionTrombi;
	private static final String GROUPE_CONTAINER_ID = "groupeContainer";
	private boolean resetTabs;
	private boolean resetLeftTreeView;
	
	private EOEditingContext gestionGroupeEc = ERXEC.newEditingContext();
	
    public TrombiPage(WOContext context) {
        super(context);
    }
    
    @Override
	public EOEditingContext edc() {
	    return gestionGroupeEc;
	}
	
    public String getSectionTrombi() {
        return sectionTrombi;
    }
    
    public void setSectionTrombi(String sectionTrombi) {
        this.sectionTrombi = sectionTrombi;
    }
    
    public String groupeContainerId() {
		return GROUPE_CONTAINER_ID;
	}
    
    public boolean isResetTabs() {
		return resetTabs;
	}
    
    public void setResetTabs(boolean resetTabs) {
		this.resetTabs = resetTabs;
	}
    
    public boolean isResetLeftTreeView() {
		return resetLeftTreeView;
	}

	public void setResetLeftTreeView(boolean resetLeftTreeView) {
		this.resetLeftTreeView = resetLeftTreeView;
	}
    
    public EOStructure getStructure() {
		return structure;
	}

	public void setStructure(EOStructure structure) {
		this.structure = structure;
	}
	
	public NSDictionary<String, EOQualifier> filters() {
		if (filters == null) {
			filters = 
				new NSMutableDictionary<String, EOQualifier>();
			EOQualifier qual = EOStructureForGroupeSpec.QUAL_GROUPES_SERVICES;
			filters.put("Services", qual);
			qual = EOStructureForGroupeSpec.QUAL_GROUPES_LABORATOIRES;
			filters.put("Labos", qual);
			qual = EOStructureForGroupeSpec.QUAL_GROUPE_FORUMS;
			filters.put("Forums", qual);
			qual = EOStructureForGroupeSpec.QUAL_GROUPE_ENTREPRISES;
			filters.put("Entreprises", qual);
		}
		return filters;
	}
	
	public WOActionResults doNuthin() {
		return null;
	}
	
	public WOActionResults onSelectGroupe() {
		setResetTabs(true);
		edc().revert();
		return null;
	}
	
	public boolean hasASelectedStructure(){
		if (getStructure() != null){
			return true;
		}
		return false;
	}
    
}