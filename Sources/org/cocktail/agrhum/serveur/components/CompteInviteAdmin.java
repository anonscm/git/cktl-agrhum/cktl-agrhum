/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.cocktail.agrhum.serveur.metier.TicketService;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXRedirect;
import er.extensions.appserver.ERXWOContext;

public class CompteInviteAdmin extends MyWOComponent {
	
	private static final long serialVersionUID = 3259725569283081630L;
    private static final String BINDING_EDC = "editingContext";
	
    private WODisplayGroup displayGroup;
    private NSArray<CktlAjaxTableViewColumn> colonnes;
    private IPersonne personne = null;
    private EOCompte selectedCompte = null;
    private EORepartStructure repartStructureInviteWifi;
    
    private EOCompte ticket;
    
	private String containerId;
	private NSArray<EOVlans> vlansAutorises;
	
    public CompteInviteAdmin(WOContext context) {
        super(context);
    }
    
    public EOEditingContext editingContext() {
        return (EOEditingContext)valueForBinding(BINDING_EDC);
    }
    
    public String containerId() {
        if (containerId == null)
            containerId = "CompteInviteAdmin"+ERXWOContext.safeIdentifierName(context(), true);
       return containerId;
    }
    
    
    /**
	 * @return the displayGroup
	 */
	public WODisplayGroup displayGroup() {
		return displayGroup;
	}
	
	private NSArray<EOCompte> allCptesInvites() {
        NSArray<EOCompte> cptesInvites = NSArray.EmptyArray;
        try {
        	cptesInvites = TicketService.findAllTickets(editingContext());
        } catch (Exception e) {
            session().addSimpleErrorMessage(e.getLocalizedMessage(), e);
        }
        return cptesInvites;
    }

	/**
	 * @param displayGroup the displayGroup to set
	 */
	public void setDisplayGroup(WODisplayGroup displayGroup) {
		this.displayGroup = displayGroup;
	}
	
	public int utilisateurPersId(){
		return applicationUser().getPersId();
	}
	
	public boolean isInviteSelected(){
		if (getPersonne() != null){
			return true;
		}
		return false;
	}
	
	public boolean hasNoCompteSelected(){
		return !isInviteSelected() && getSelectedCompte() == null;
	}
    
	public IPersonne getPersonne() {
		return personne;
	}

	public void setPersonne(IPersonne personne) {
		this.personne = personne;
		doNothing();
	}

	public EOCompte getSelectedCompte() {
		return selectedCompte;
	}

	public void setSelectedCompte(EOCompte selectedCompte) {
		this.selectedCompte = selectedCompte;
	}

	public NSArray<EOVlans> getVlansAutorises() {
		if (vlansAutorises == null) {
			return new NSArray<EOVlans>(EOVlans.fetchByKeyValue(edc(), EOVlans.C_VLAN_KEY, EOVlans.VLAN_X));
		}
		return vlansAutorises;
	}
	public WOActionResults onCreerPersonne() {
		if (getPersonne() instanceof EOStructure) {
		} else if (getPersonne() instanceof EOIndividu) {
			EOIndividu individu = (EOIndividu) getPersonne();
			if (individu.toCivilite() == null) {
				NSArray<EOCivilite> civilites = EOCivilite.fetchAll(editingContext());
				individu.setToCiviliteRelationship((EOCivilite) civilites.objectAtIndex(0));
			}
		}

		if (getPersonne().persIdCreation() == null) {
			getPersonne().setPersIdCreation(utilisateurPersId());
		}
		if (getPersonne().persIdModification() == null) {
			getPersonne().setPersIdModification(utilisateurPersId());
		}
		return doNothing();
	}
	
	public WOActionResults annuler() {
		editingContext().revert();
		ERXRedirect redirect = (ERXRedirect)pageWithName(ERXRedirect.class.getName());
		WOComponent accueil = pageWithName(Accueil.class.getName());
		redirect.setComponent(accueil);
		return redirect;
	}
	
	public WOActionResults sauvegarderEnBase(){
		if ( getPersonne().toComptes(EOCompte.QUAL_CPT_VLAN_X) != null && getPersonne().toComptes(EOCompte.QUAL_CPT_VLAN_X).count() > 0){
			try {
				EOQualifier qual = EORepartStructure.C_STRUCTURE.eq("30").and(EORepartStructure.PERS_ID.eq(getPersonne().persId()));
				EORepartStructure repartStructureExistante = EORepartStructure.fetchByQualifier(editingContext(), qual);
				if (repartStructureInviteWifi == null && repartStructureExistante == null){
					EOStructure groupeInvitesWifi = EOStructureForGroupeSpec.getGroupeForParamKey(
		                editingContext(), EOStructureForGroupeSpec.ANNUAIRE_INVITE_WIFI_KEY);
					repartStructureInviteWifi = EORepartStructure.creerInstanceSiNecessaire(editingContext(), getPersonne(), groupeInvitesWifi, utilisateurPersId());
					EOStructureForGroupeSpec.affecterPersonneDansGroupe(editingContext(), getPersonne(), groupeInvitesWifi, applicationUser().getPersId());
				}
				
//				EOStructureForGroupeSpec.affecterPersonneDansGroupe(editingContext(), getPersonne(), groupeInvitesWifi, applicationUser().getPersId());
//				TicketService.ajouterInviteWifi(editingContext(), applicationUser().getPersId(), getPersonne());
				editingContext().saveChanges();
//				session().addSimpleInfoMessage(localizer().localizedTemplateStringForKeyWithObject("invite.valid", getPersonne()), null);
				session().addSimpleInfoMessage(session().localizer().localizedStringForKey("confirmation"), null);
				ERXRedirect redirect = (ERXRedirect)pageWithName(ERXRedirect.class.getName());
				WOComponent accueil = pageWithName(Accueil.class.getName());
				redirect.setComponent(accueil);
				return redirect;
			} catch (NSValidation.ValidationException e) {
				logger().info(e.getMessage(), e);
				session().addSimpleErrorMessage(e.getLocalizedMessage(), e);
				context().response().setStatus(500);
			} catch (Exception e) {
				editingContext().revert();
				logger().warn(e.getMessage(), e);
				context().response().setStatus(500);
				session().addSimpleErrorMessage("Une erreur est survenue lors de l'enregistrement en base", e);
			} 
		}
		
		return null;
	}

	public WOActionResults onSelection() {
//    	setWillRefreshEditor(true);
        return null;
    }

	
	/**
	 * retourne true si l'utilisateur peut gérer les comptes
	 */
	public boolean hasDroitGererComptes(){
		return applicationUser().hasDroitCompteModification();
	}

	public boolean isEditing(){
		return hasDroitGererComptes();
	}
	
	public boolean isReadOnly(){
		return !isEditing();
	}
}