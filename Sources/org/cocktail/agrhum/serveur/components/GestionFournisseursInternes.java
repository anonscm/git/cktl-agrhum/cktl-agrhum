/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOAdaptorChannel;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EOStoredProcedure;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxUpdateContainer;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderOrgan;
import org.cocktail.fwkcktlpersonne.common.eospecificites.FournisseurInterneServices;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.ASelectComponent;
import org.cocktail.fwkcktlpersonneguiajax.serveur.controleurs.NotificationCtrl;

import com.webobjects.foundation.NSArray;

/**
 * Composant de gestion des structures fournisseurs internes
 * 
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 * 
 */
public class GestionFournisseursInternes extends MyWOComponent {
	private static final long serialVersionUID = 5040385088251442576L;

	private static final Logger LOG = Logger.getLogger(GestionFournisseursInternes.class);

	private static final String FOURNIS_INT_CONTAINER_ID = "fournisInternesContainer";
	private static final String STRUCTUREOB = ASelectComponent.CURRENT_OBJ_KEY;

	private String sectionFournisseurInterne;
	private EOEditingContext editingContext;
	private EOStructure structureOB;
	private NSArray<EOStructure> allStructuresOB;
	private boolean resetLeftTreeView;

	private NSArray<CktlAjaxTableViewColumn> colonnes;

	private String structureOBFilter;

	public GestionFournisseursInternes(WOContext context) {
		super(context);
	}

	public String getSectionFournisseurInterne() {
		return sectionFournisseurInterne;
	}

	public void setSectionFournisseurInterne(String sectionFournisseur) {
		this.sectionFournisseurInterne = sectionFournisseur;
	}

	/**
	 * @return the editingContext
	 */
	public EOEditingContext editingContext() {
		if (editingContext == null) {
			editingContext = ERXEC.newEditingContext();
		}
		return editingContext;
	}

	/**
	 * @return the fournisInternesContainerId
	 */
	public String getFournisInternesContainerId() {
		return FOURNIS_INT_CONTAINER_ID;
	}

	/**
	 * @return the structureOB
	 */
	public EOStructure getStructureOB() {
		return structureOB;
	}

	/**
	 * @param structureOB the structureOB to set
	 */
	public void setStructureOB(EOStructure structureOB) {
		AjaxUpdateContainer.updateContainerWithID(getFournisInternesContainerId(), context());
		this.structureOB = structureOB;
	}

	public WOActionResults onSelectStructureOB() {
//		setResetTabs(true);
		edc().revert();
		return null;
	}

	public boolean isResetLeftTreeView() {
		return resetLeftTreeView;
	}

	public void setResetLeftTreeView(boolean resetLeftTreeView) {
		this.resetLeftTreeView = resetLeftTreeView;
	}

	private void prepareRefreshLeftTreeView() {
		NSMutableDictionary<String, Object> userInfo = new NSMutableDictionary<String, Object>();
		userInfo.setObjectForKey(edc(), "edc");
		NotificationCtrl.postNotificationForOnAnnulerNotification(this, userInfo);
		setResetLeftTreeView(true);
	}

	/**
	 * Structures de type organisation budgetaires Pour qu'une structure soit considérée comme organisation budgétataire, il faut 1) qu'elle fasse partie des
	 * structures avec organisation dans jefyAdmin (table jefy_admin.organ) 2) qu'elle soit dans groupe de type "ligne budgetaire"
	 * @return the allStructuresOB
	 */
	public NSArray<EOStructure> getAllStructuresOB() {
		if (allStructuresOB == null) {
			NSArray<EOStructure> allStructures = FinderOrgan.getAllStructureAvecOrgan(editingContext());
			allStructuresOB = new NSMutableArray<EOStructure>();
			for (EOStructure structure : allStructures) {
				NSArray<String> groupesCodes = (NSArray<String>) structure.toRepartTypeGroupes().valueForKey(EORepartTypeGroupe.TGRP_CODE_KEY);
				if (groupesCodes.contains(EOTypeGroupe.TGRP_CODE_LB) && structure.isInterneEtablissement()) {
					allStructuresOB.add(structure);
				}
			}
		}

		return allStructuresOB;
	}

	/**
	 * @param allStructuresOB the allStructuresOB to set
	 */
	public void setAllStructuresOB(NSArray allStructuresOB) {
		this.allStructuresOB = allStructuresOB;
	}

	public NSArray<CktlAjaxTableViewColumn> getColonnes() {
		if (colonnes == null) {
			NSMutableArray<CktlAjaxTableViewColumn> colTmp = new NSMutableArray<CktlAjaxTableViewColumn>();
			// Colonne Libelle
			CktlAjaxTableViewColumn col = new CktlAjaxTableViewColumn();
			col.setLibelle("Structures pouvant jouer le rôle de fournisseur interne");
			col.setOrderKeyPath(EOStructure.LL_STRUCTURE_KEY);
			String keyPath = ERXQ.keyPath(STRUCTUREOB, EOStructure.LL_STRUCTURE_KEY);
			CktlAjaxTableViewColumnAssociation ass = new CktlAjaxTableViewColumnAssociation(keyPath, "");
			col.setAssociations(ass);
			colTmp.add(col);
//			// Colonne Libelle long
//			col = new CktlAjaxTableViewColumn();
//			col.setLibelle("Libellé");
//			col.setOrderKeyPath(LL_KEY);
//			keyPath = ERXQ.keyPath(CORPS, LL_KEY);
//			ass = new CktlAjaxTableViewColumnAssociation(
//					keyPath, "");
//			col.setAssociations(ass);
//			colTmp.add(col);
			colonnes = colTmp.immutableClone();
		}
		return colonnes;
	}

	/**
	 * déclare l'ensemble des structures disponibles comme étant des fournisseurs internes
	 * @return null (reste sur la page
	 */
	public WOActionResults declareStructuresCommeFournisseursInternes() {
		FournisseurInterneServices fournisseurInterneServices = new FournisseurInterneServices();
		for (EOStructure structure : allStructuresOB) {
			try {
				if (!fournisseurInterneServices.isStructureFournisseurInterne(structure)) {
					fournisseurInterneServices.declareFournisseurInterne(structure, session().applicationUser());
					editingContext().saveChanges();
					session().addSimpleInfoMessage(localizer().localizedStringForKey("confirmation"), null);
				}
			} catch (NSForwardException e) {
				editingContext().revert();
				LOG.error(e.getMessage(), e);
				context().response().setStatus(500);
				if (e.originalException() != null) {
					session().addSimpleErrorMessage(e.originalException().getMessage(), e.originalException());
				}
			} catch (Exception e1) {
				editingContext().revert();
				LOG.error(e1.getMessage(), e1);
				context().response().setStatus(500);
				session().addSimpleErrorMessage(e1.getMessage(), e1);
			}
		}
		return null;
	}

	/**
	 * @return the structureOBFilter
	 */
	public String getStructureOBFilter() {
		return structureOBFilter;
	}

	/**
	 * @param structureOBFilter the structureOBFilter to set
	 */
	public void setStructureOBFilter(String structureOBFilter) {
		this.structureOBFilter = structureOBFilter;
	}

	public EOQualifier getStructureOBFilterQualifier() {
		return ERXQ.contains(EOStructure.LL_STRUCTURE_KEY, getStructureOBFilter());
	}

}