/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl;
import org.cocktail.agrhum.serveur.export.ExportEngine;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSDictionary;

import er.extensions.components.ERXComponent;
import er.extensions.foundation.ERXDictionaryUtilities;

public class InfosIndividuExportExcel extends ERXComponent {
/**
* 
*/
	private static final long serialVersionUID = 11111111111111L;
	
	//TODO a effacer ?
	private SelectionInfosIndividusCtrl ctrl;
	private String uneInfo;
	private EORepartStructure uneRepartStructure;
	
	private NSArray<EORepartStructure> listeSelection;
	private NSArray<String> selectedInfosChoisies;
	
	private String nomFichier;
	
	private ExportEngine engine = new ExportEngine();
	
    public InfosIndividuExportExcel(WOContext context) {
        super(context);
        
    }


	public NSArray<EORepartStructure> getListeSelection() {
		return listeSelection;
	}

	public void setListeSelection(NSArray<EORepartStructure> listeSelection) {
		this.listeSelection = listeSelection;
	}

	public NSArray<String> getSelectedInfosChoisies() {
		return selectedInfosChoisies;
	}

	public void setSelectedInfosChoisies(NSArray<String> selectedInfosChoisies) {
		this.selectedInfosChoisies = selectedInfosChoisies;
	}
	
	public String getNomFichier() {
		nomFichier = "recap_infos_individus_" + DateCtrl.currentDateHeureString() + ".xls";
		return nomFichier;
	}

	public String getUneInfo() {
		return uneInfo;
	}

	public void setUneInfo(String uneInfo) {
		this.uneInfo = uneInfo;
	}

	public EORepartStructure getUneRepartStructure() {
		return uneRepartStructure;
	}

	public void setUneRepartStructure(EORepartStructure uneRepartStructure) {
		this.uneRepartStructure = uneRepartStructure;
	}


	/**
	 * @return the ctrl
	 */
	public SelectionInfosIndividusCtrl ctrl() {
		return ctrl;
	}
	/**
	 * @param ctrl
	 *            the ctrl to set
	 */
	public void setCtrl(SelectionInfosIndividusCtrl ctrl) {
		this.ctrl = ctrl;
		engine.setEditingContext(ctrl.editingContext());
	}
    
    public NSDictionary<?,?> excelStylesData() {
    	NSDictionary <?, ?> dict = ERXDictionaryUtilities.dictionaryFromPropertyList("ExcelStyles", NSBundle.mainBundle());
    	return dict;
    }
    
	public String noString(){
		return "---";
	}
	
	/* *********************************************************************************************************************************** */
	
	public String getInfoIndividu(){
		
		IPersonne personne = uneRepartStructure.toPersonneElt();
		
		String info;
		
		if (personne == null) {
			return noString();
		}
		
		if (personne.isIndividu()) {
			info = engine.getInfoPerso(getUneInfo(), (EOIndividu) personne, noString());
		} else {
			info = engine.getInfoPerso(getUneInfo(), (EOStructure) personne, noString());
		}
		
		return info;
	}
}