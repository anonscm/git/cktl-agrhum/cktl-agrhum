/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.cocktail.agrhum.serveur.metier.PersonneSauvegardeService;
import org.cocktail.agrhum.serveur.metier.SauvegardeService;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.server.util.CompteService;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.sync.ref.SynchroPasswordPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOGeneralAdaptorException;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.appserver.ERXRedirect;

public class EditIndividuPage extends MyWOComponent {
	private static final long serialVersionUID = 2751064448292949068L;

	CktlConfig config = application().config();

	public EditIndividuPage(WOContext context) {
		super(context);
	}

	private EOIndividu individu;

	/**
	 * @return the individu
	 */
	public EOIndividu getIndividu() {
		return individu;
	}

	/**
	 * @param individu the individu to set
	 */
	public void setIndividu(EOIndividu individu) {
		this.individu = individu.localInstanceIn(edc());
	}

	public boolean isIndividuSelected() {
		return getIndividu() != null;
	}

	public boolean shouldBeSynchronized() {
		// On synchronise si besoin
		// return ("YES".equals(application().config().stringForKey("USE_SYNCREF_PWD")));
		return ("YES".equals(config.stringForKey("USE_SYNCREF_PWD")));

	}

	public WOActionResults annuler() {
		ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
		WOComponent accueil = pageWithName(Accueil.class.getName());
		redirect.setComponent(accueil);
		return redirect;
	}

	public WOActionResults valider() {
		if (shouldBeSynchronized()) {
			try {
				SynchroPasswordPersonne.synchroPasswordPersonne(Integer.toString(getIndividu().persId()));
			} catch (Exception e) {
				mySession().addSimpleErrorMessage("LDAP",
				        "Mot de passe changé dans la base mais il y a eu une erreur du serveur de synchronisation :\n" + e.getMessage());
			}
		}
		save();
		return null;
	}

	private void save() {
		try {
			SauvegardeService service = new PersonneSauvegardeService();
			service.save(edc(), getIndividu(), applicationUser().hasDroitGrhumCreateur());
			session().addSimpleInfoMessage(session().localizer().localizedStringForKey("confirmation"), null);
		} catch (NSValidation.ValidationException e) {
			logger().info(e.getMessage(), e);
			session().addSimpleErrorMessage(e.getLocalizedMessage(), e);
			context().response().setStatus(500);
		} catch (EOGeneralAdaptorException e) {
			edc().revert();
			logger().warn(e.getMessage(), e);
			context().response().setStatus(500);
			session().addSimpleErrorMessage("Une erreur est survenue lors de l'enregistrement en base", e);
		} finally {
			session().reactivationTrigger();
		}
		
		
	}

	public String getIndividuContainerId() {
		return getComponentId() + "_individuContainer";
	}
}