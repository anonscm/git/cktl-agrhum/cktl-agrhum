/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.agrhum.serveur.components.assistants.modules;

import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;

import com.webobjects.appserver.WOContext;

public class ModulePersonneAdmin extends ModuleAssistant implements IModuleAssistant {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1615524002844546222L;

	public ModulePersonneAdmin(WOContext context) {
        super(context, null);
    }
	
	public Boolean allowEditNoInsee() {
		
		if (personne().isIndividu()) {
			// Si l'individu est un personnel et que l'on a précisé via GRHUM_RH (le paramètre du User GRHUM) que l'on utilise Mangue
			// pour la RH.
			// Alors seule la consultation du N° INSEE est possible pour un Personnel dans AGRHUM : OBLIGATION d'utiliser Mangue pour cela
//			if ("OUI".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.APPLICATION_MANGUE_INSTALLED))
			if ("OUI".equals(EOGrhumParametres.parametrePourCle(personne().editingContext(), EOGrhumParametres.PARAM_GRHUM_RH, "OUI"))
					&& ((EOIndividu)personne()).isPersonnel() ) {
				return false;
			}
			// Si l'individu est un étudiant et que l'on a précisé via GRHUM_RH (le paramètre du User GRHUM) que l'on utilise Scolarix
			// pour la Scolarité.
			// Alors seule la consultation du N° INSEE est possible pour un Etudiant dans AGRHUM : OBLIGATION d'utiliser Scolarix
			// pour cela.
//			if ("OUI".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.APPLICATION_SCOLARIX_INSTALLED))
			if ("OUI".equals(EOGrhumParametres.parametrePourCle(personne().editingContext(), EOGrhumParametres.PARAM_GRHUM_PEDA, "OUI"))
					&& ((EOIndividu)personne()).isEtudiant() ) {
				return false;
			}
		}
		return true;
	}

	public Boolean allowEditEtatCivil() {
		
		if (personne().isIndividu()) {
			// Si l'individu est un personnel et que l'on a précisé via GRHUM_RH (le paramètre du User GRHUM) que l'on utilise Mangue
			// pour la RH.
			// Alors seule la consultation de l'état civil est possible pour un Personnel dans AGRHUM : OBLIGATION d'utiliser Mangue pour cela
//			if ("OUI".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.APPLICATION_MANGUE_INSTALLED))
			if ("OUI".equals(EOGrhumParametres.parametrePourCle(personne().editingContext(), EOGrhumParametres.PARAM_GRHUM_RH, "OUI"))
					&& ((EOIndividu)personne()).isPersonnel() ) {
				return false;
			}
			// Si l'individu est un étudiant et que l'on a précisé via GRHUM_RH (le paramètre du User GRHUM) que l'on utilise Scolarix
			// pour la Scolarité.
			// Alors seule la consultation de l'état civil est possible pour un Etudiant dans AGRHUM : OBLIGATION d'utiliser Scolarix
			// pour cela.
//			if ("OUI".equals(FwkCktlPersonne.paramManager.getParam(FwkCktlPersonneParamManager.APPLICATION_SCOLARIX_INSTALLED))
			if ("OUI".equals(EOGrhumParametres.parametrePourCle(personne().editingContext(), EOGrhumParametres.PARAM_GRHUM_PEDA, "OUI"))
					&& ((EOIndividu)personne()).isEtudiant() ) {
				return false;
			}
		}
		return true;
		
	}

	public Boolean showEtatCivil() {
		return myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PARAM_FOURNIS_FORM_SHOWNAISSANCE);
	}

	/**
	 * Indique si il faut afficher le n° Insee
	 * @return
	 */
	public Boolean showNoInsee() {
		return myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PARAM_FOURNIS_FORM_SHOWINSEE);
	}
		
}
