/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import com.webobjects.appserver.WOContext;

import org.cocktail.fwkcktldroitsutils.common.util.AUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;

public class FournisseurInfosAdmin extends MyWOComponent {
	private static final long serialVersionUID = 5084193737050877657L;
	
    public static final String BINDING_FOURNISSEUR = "fournisseur";


	public FournisseurInfosAdmin(WOContext context) {
        super(context);
    }
    
    public String getFouCode() {
        return getFournisseur().fouCode() == null ? "n/a" : getFournisseur().fouCode();
    }
    
    private EOFournis getFournisseur() {
        return (EOFournis)valueForBinding(BINDING_FOURNISSEUR);
    }
    
    public boolean fouTypeFournisseurSelected() {
        return getFournisseur().isTypeFournisseur();
    }
    
    public void setFouTypeFournisseurSelected(Boolean val) {
        if (val.booleanValue()) {
            getFournisseur().setFouType(EOFournis.FOU_TYPE_FOURNISSEUR);
        }
    }
    
    public boolean fouTypeClientSelected() {
        return getFournisseur().isTypeClient();
    }
    
    public void setFouTypeClientSelected(Boolean val) {
        if (val.booleanValue()) {
            getFournisseur().setFouType(EOFournis.FOU_TYPE_CLIENT);
        }
    }
    
    public boolean fouTypeTiersSelected() {
        return getFournisseur().isTypeTiers();
    }
    
    public void setFouTypeTiersSelected(Boolean val) {
        if (val.booleanValue()) {
            getFournisseur().setFouType(EOFournis.FOU_TYPE_TIERS);
        }
    }
    
    public boolean getIsEtranger() {
        return getFournisseur().isEtranger();
    }
    
    public void setIsEtranger(Boolean isEtranger) {
        getFournisseur().setIsEtranger(isEtranger);
    }
    
    public Boolean canEditFournisseur() {
    	return applicationUser().hasDroitModificationEOFournis(getFournisseur(), AUtils.currentExercice()) 
    			|| applicationUser().hasDroitCreationEOFournis(null, AUtils.currentExercice());
    }
    
    public boolean cannotEditFournisseur() {
    	return ! canEditFournisseur();
    }
    
    public Boolean allowEditNoInsee() {
		if (getFournisseur().isIndividu()) {
			return !(getFournisseur().toIndividu().isPersonnel() || getFournisseur().toIndividu().isEtudiant());
		}
		return false;
	}

	public Boolean allowEditEtatCivil() {
		if (getFournisseur().isIndividu()) {
			return !(getFournisseur().toIndividu().isPersonnel() || getFournisseur().toIndividu().isEtudiant());
		}
		return false;
	}

	public Boolean showEtatCivil() {
		return myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PARAM_FOURNIS_FORM_SHOWNAISSANCE);
	}

	public Boolean showNoInsee() {
		return myApp().config().booleanForKey(FwkCktlPersonneGuiAjaxParamManager.PARAM_FOURNIS_FORM_SHOWINSEE);
	}
}