/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering;
import er.extensions.foundation.ERXArrayUtilities;

public class PersonneRechercheGlobale extends MyWOComponent {
	private static final long serialVersionUID = -8648012745496408688L;

	private String srchPatern;
	private IPersonne selectedPersonne;
	private WODisplayGroup displayGroup;

	public PersonneRechercheGlobale(WOContext context) {
		super(context);
	}
	
	public String getGlobalSrchContainerId() {
		return getComponentId() + "_" + "GlobalSrchContainer";
	}

	public String srchPaternFieldId() {
		return getComponentId() + "_" + "TFSrchPatern";
	}

	public String getSrchPatern() {
		if (srchPatern != null) {
			srchPatern = srchPatern.trim();
		}
		return srchPatern;
	}

	public void setSrchPatern(String srchPatern) {
		this.srchPatern = srchPatern;
	}

//	public String btRechercherOnClick() {
//		return "jsOnRechercher(null);";
//	}

	public String getSrchFormId() {
		return getComponentId() + "_" + "GlobalSrchForm";
	}

	public WOActionResults doGlobalSearch() {
		try {
			NSArray res = NSArray.EmptyArray;
			displayGroup().setObjectArray(res);
//			checkSaisie();
			res = getResultats();
			displayGroup().setObjectArray(res);
		} catch (Exception e) {
			logger().error(e.getMessage(), e);
			session().addSimpleErrorMessage(e.getLocalizedMessage(), e);
		}
		return null;
	}

	public String getGlobalSearchResultModalID() {
		return getComponentId() + "_" + "GlobalSrchResultModal";
	}

	public Boolean hasResults() {
		return Boolean.valueOf(displayGroup().allObjects() != null && displayGroup().allObjects().count() > 0);
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedPersonne((IPersonne) group.selectedObject());
		}
	}
	
	public IPersonne getSelectedPersonne() {
		return selectedPersonne;
	}

	public void setSelectedPersonne(IPersonne personne) {
		selectedPersonne = personne;
		// setValueForBinding(personne, BINDING_selectedPersonne);
		// setWantRefreshGroupes(Boolean.TRUE);
	}

	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			// displayGroup = (WODisplayGroup)
			// valueForBinding(BINDING_displayGroup);
			// if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
			// if (canSetValueForBinding(BINDING_displayGroup)) {
			// setValueForBinding(displayGroup, BINDING_displayGroup);
			// }
		}
		displayGroup.setDelegate(new DgDelegate());
		// }
		return displayGroup;
	}

	public Integer utilisateurPersId() {
		return applicationUser().getPersId();
	}

	public NSArray getResultats() {
		
		NSArray<IPersonne> resultats = new NSMutableArray<IPersonne>();
		NSArray<EOStructure> structures = new NSMutableArray<EOStructure>();
		NSArray<EOIndividu> individus = new NSMutableArray<EOIndividu>();
		
		String srchPatern = getSrchPatern();
		
		if (!MyStringCtrl.isEmpty(srchPatern)) {
			srchPatern = MyStringCtrl.replace(srchPatern, " ", "*");
		}
		
		
		// struture externe by name
		structures.addAll(EOStructure.structuresExternesByNameAndSigleAndSiretAndFouCode(edc(), srchPatern, null, null, null,
				null, application().getSearchFetchLimit()));
		// structure interne by name
		structures.addAll(EOStructure.structuresInternesByNameAndSigleAndSiretAndFouCode(edc(), srchPatern, null, null, null,
				null, application().getSearchFetchLimit()));
		
		
		structures = ERXArrayUtilities.arrayWithoutDuplicates(structures);
		
		structures = EOStructure.filtrerLesStructuresNonAffichables(structures, edc(), applicationUser().getPersId());
		
		individus.addAll(EOIndividu.individusWithNameLikeAndFirstNameEquals(edc(), srchPatern, null, application().getSearchFetchLimit()));
		individus.addAll(EOIndividu.individusWithFirstNameLike(edc(), srchPatern, null, application().getSearchFetchLimit()));
		individus.addAll(EOIndividu.individusWithMiddleNameLike(edc(), srchPatern, null, application().getSearchFetchLimit()));
		individus.addAll(EOIndividu.individusWithLoginEquals(edc(), srchPatern, null, application().getSearchFetchLimit()));
		
		individus = ERXArrayUtilities.arrayWithoutDuplicates(individus);
		
		resultats.addAll(structures);
		resultats.addAll(individus);
		
		return ERXSortOrdering.sortedArrayUsingKeyOrderArray(resultats, ERXS.ascInsensitives(IPersonne.NOM_PRENOM_RECHERCHE_KEY));
	}

	public WOActionResults doSelection() {
		WOActionResults nextPage = null;
		if (getSelectedPersonne()!=null) {
			if (getSelectedPersonne().isStructure()) {
				nextPage = (EditStructurePage) pageWithName(EditStructurePage.class.getName());
				((EditStructurePage)nextPage).setStructure((EOStructure)getSelectedPersonne());
				((EditStructurePage)nextPage).setResetTabs(true);
			} else {
				nextPage = (EditIndividuPage) pageWithName(EditIndividuPage.class.getName());
				((EditIndividuPage)nextPage).setIndividu((EOIndividu)getSelectedPersonne());
			}
		}
		return nextPage;
	}
	
	public WOActionResults onCloseResultModal() {
		resetComponent();
        return null;
    }

	protected void resetComponent() {
		setSelectedPersonne(null);
		setSrchPatern(null);
		displayGroup().setObjectArray(NSArray.EmptyArray);
	}

	public String onFormSrchObserverComplete() {
//		return "function(oC){" + getGlobalSrchContainerId() + "Update();" + listeResContainerId() + "Update();" + (detailResContainerId() != null ? detailResContainerId() + "Update();" : "") + "}";
		
//		String onSuccessAddition = "openCAW_"+getGlobalSearchResultModalID()+"($('"+getId(context())+"').readAttribute('title')";
		
//		return "function(oC){" + getGlobalSrchContainerId() + "Update();" + "openCAW_" + getGlobalSearchResultModalID() + "();" + "}";
		return "function(){" + "openCAW_" + getGlobalSearchResultModalID() + "();" + "}";

	}

	public String onFormSrchObserverSuccess() {
		return "function(oC){" + "openCAW_" + getGlobalSearchResultModalID() + "();" + "return false;"+ "}";
	}

}