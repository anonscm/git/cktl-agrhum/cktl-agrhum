/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;

public class DictionnaireDuSchema extends MyWOComponent {
	
	private static final long serialVersionUID = 7945102018433119695L;
	
	private static final String DICTIONNAIRE_CONTAINER_ID = "dictionnaireSchemaContainer";
	static public Logger LOG = Logger.getLogger(DictionnaireDuSchema.class);
	
	private String sectionDictionnaireSchema;
	private String nomSchema;
	private String filtreExclusion;
	private String filtreRecherche;
	
	private boolean activationRapport = false;
	private boolean canSubmit = false;
	
	private ReporterAjaxProgress reporterProgress;
	private String reportFilename;
	private CktlAbstractReporter reporter;
	
    public DictionnaireDuSchema(WOContext context) {
        super(context);
    }
    
    
	public boolean isActivationRapport() {
		return activationRapport;
	}

	public void setActivationRapport(boolean activationRapport) {
		this.activationRapport = activationRapport;
	}
	
	public boolean isPrintable(){
		return !activationRapport;
	}
	
	public boolean isCanSubmit() {
		return canSubmit;
	}

	public void setCanSubmit(boolean canSubmit) {
		this.canSubmit = canSubmit;
	}


	public String getFiltreExclusion() {
		return filtreExclusion;
	}

	public void setFiltreExclusion(String filtreExclusion) {
		this.filtreExclusion = filtreExclusion;
	}

	public String getFiltreRecherche() {
		return filtreRecherche;
	}

	public void setFiltreRecherche(String filtreRecherche) {
		this.filtreRecherche = filtreRecherche;
	}

	public String getNomSchema() {
		return nomSchema;
	}

	public void setNomSchema(String nomSchema) {
		this.nomSchema = nomSchema;
	}

	public String getSectionDictionnaireSchema() {
		return sectionDictionnaireSchema;
	}

	public void setSectionDictionnaireSchema(String sectionDictionnaireSchema) {
		this.sectionDictionnaireSchema = sectionDictionnaireSchema;
	}
    
	/**
	 * @return the parametersContainerId
	 */
	public String getDictionnaireSchemaContainerId() {
		return DICTIONNAIRE_CONTAINER_ID;
	}
	
	public WOActionResults controleEtActivationBtRapport(){
		if (MyStringCtrl.isEmpty(nomSchema)){
			setNomSchema("GRHUM");
		}
		if (MyStringCtrl.isEmpty(filtreExclusion)){
			setFiltreExclusion("");
		}
		if (MyStringCtrl.isEmpty(filtreRecherche)){
			setFiltreRecherche("%");
		}
		setActivationRapport(true);
		setCanSubmit(true);
		return doNothing();
	}

	public WOActionResults remiseAZero(){
		
		setNomSchema(null);
		setFiltreExclusion(null);
		setFiltreRecherche(null);
		
		setActivationRapport(false);
		setCanSubmit(false);
		
		return doNothing();
	}
	
	public CktlAbstractReporter getReporter() {
        return reporter;
    }

	public CktlAbstractReporterAjaxProgress getReporterProgress() {
        return reporterProgress;
    }
	
	public String getReportFilename() {
        return reportFilename;
    }
	
	public void setReportFilename(String reportFilename) {
        this.reportFilename = reportFilename;
    }
    
	
	public static class ReporterAjaxProgress extends CktlAbstractReporterAjaxProgress implements org.cocktail.reporting.server.jrxml.IJrxmlReportListener {

        public ReporterAjaxProgress(int maximum) {
            super(maximum);
        }

	}
	
	public WOActionResults onPrint() {
		reporterProgress = new ReporterAjaxProgress(100);

		reportFilename = "ImpressionDictionnaireSchemaSelectionne.pdf";
		reporter = reporter(reporterProgress);

		return doNothing();
	}
	
	public CktlAbstractReporter reporter(IJrxmlReportListener listener) {

		 String jasperFileName = "report/Dictionnaire/cktl_oracle_user_schema.jasper";

		 String recordPath = "user";
		 JrxmlReporter jr = null;
		 try {
			 Connection connection = application().getJDBCConnection();
//			 Connection connection = (Application)Application.getApplication().getJDBCConnection();
			 jr = new JrxmlReporter();
			 Map<String, Object> parameters = new HashMap<String, Object>();
			 parameters.put("ORACLE_USER_NAME",getNomSchema().toUpperCase() );
			 parameters.put("TABLE_NOT_LIKE_MASK", getFiltreExclusion().toUpperCase() );
			 parameters.put("TABLE_LIKE_MASK", getFiltreRecherche().toUpperCase() );
			 jr.printWithThread("Impression du dictionnaire d'un User", connection,null, pathForJasper(jasperFileName), parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, true,listener);
		 }
		 catch (Throwable e) {
			 	LOG.error("Une erreur s'est produite durant l'edition du dictionnaire d'un User", e);
		 }
		 
		 return jr;

	}

	protected String pathForJasper(String reportPath) {
        ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
        URL url = rsm.pathURLForResourceNamed(reportPath, "app", null);
        return url.getFile();
    }
	
}