/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import java.util.ArrayList;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;

public class ViewMembresGroupe extends MyWOComponent {

	private static final long serialVersionUID = 3670400336991142219L;
	public static final String BINDING_DG = "displayGroup";
	public static final String BINDING_TBV_HEIGHT = "tbvHeight";
	public static final String BINDING_TBV_WIDTH = "tbvWidth";
	public static final String BINDING_TBV_DISPLAY_FILTER = "displayFilter";
	public final String BINDING_ACTION_SELECTION_MEMBRE_CHANGE = "selectionMembreChange";

	public static final String LIBELLE_STRUCTURE_KP = EORepartStructure.TO_PERSONNE_ELT_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY;
	public static final String LIBELLE_ROLE_KP = "roles";
	
	private static final String LABEL_COLONNE_ROLE = "Rôle(s) au sein du groupe";
	private static final String LABEL_COLONNE_NOM = "Nom";

	private String filtreMembres;
	private NSArray<CktlAjaxTableViewColumn> colonnes;
	private EORepartStructure currentRepartStructure;

	public ViewMembresGroupe(WOContext context) {
		super(context);
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public WOActionResults filtrerMembres() {
		EOQualifier qual = ERXQ.contains(EORepartStructure.TO_PERSONNE_ELT_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY, filtreMembres);
		getMembresDisplayGroup().setQualifier(qual);
		getMembresDisplayGroup().updateDisplayedObjects();
		return null;
	}

	public WOActionResults resetFiltre() {
		setFiltreMembres(null);
		getMembresDisplayGroup().setQualifier(null);
		getMembresDisplayGroup().updateDisplayedObjects();
		return null;
	}

	public WOActionResults refreshToolbar() {
		AjaxUpdateContainer.updateContainerWithID(toolbarContId(), context());

		if (hasBinding(BINDING_ACTION_SELECTION_MEMBRE_CHANGE)) {
			return performParentAction(stringValueForBinding(BINDING_ACTION_SELECTION_MEMBRE_CHANGE, null));
		} else {
			return doNothing();
		}
	}

	public NSArray<CktlAjaxTableViewColumn> colonnes() {
		if (colonnes == null) {
			CktlAjaxTableViewColumn colNom = new CktlAjaxTableViewColumn();
			colNom.setLibelle(LABEL_COLONNE_NOM);
			CktlAjaxTableViewColumnAssociation assoNom = new CktlAjaxTableViewColumnAssociation("currentRepartStructure." + LIBELLE_STRUCTURE_KP, "Aucun libellé");
			colNom.setAssociations(assoNom);

			CktlAjaxTableViewColumn colRole = new CktlAjaxTableViewColumn();
			colRole.setLibelle(LABEL_COLONNE_ROLE);
			CktlAjaxTableViewColumnAssociation assoRole = new CktlAjaxTableViewColumnAssociation("currentRepartStructure." + LIBELLE_ROLE_KP, "");
			colRole.setAssociations(assoRole);

			NSMutableArray<CktlAjaxTableViewColumn> tabColonnes=new NSMutableArray<CktlAjaxTableViewColumn>();
			tabColonnes.add(colNom);
			tabColonnes.add(colRole);
			colonnes = tabColonnes;
		}
		return colonnes;
	}

	public boolean hasMembres() {
		return getMembresDisplayGroup().allObjects().count() > 0;
	}

	public ERXDisplayGroup<EORepartStructure> getMembresDisplayGroup() {
		return (ERXDisplayGroup<EORepartStructure>) valueForBinding(BINDING_DG);
	}

	// public String getNbreMembres(){
	// return (String) getMembresDisplayGroup().allObjects().count();
	// }

	public String tbvHeight() {
		return stringValueForBinding(BINDING_TBV_HEIGHT, "200");
	}

	public String tbvWidth() {
		return stringValueForBinding(BINDING_TBV_WIDTH, "auto");
	}

	public boolean hasFiltre() {
		return booleanValueForBinding(BINDING_TBV_DISPLAY_FILTER, Boolean.TRUE);
	}

	public String membresTbvId() {
		return "MemTbv_" + uniqueDomId();
	}

	public String membresContId() {
		return "MemCont_" + uniqueDomId();
	}

	public String toolbarContId() {
		return "ToolbarCont_" + uniqueDomId();
	}

	public String getFiltreMembres() {
		return filtreMembres;
	}

	public void setFiltreMembres(String filtreMembres) {
		this.filtreMembres = filtreMembres;
	}

	public EORepartStructure getCurrentRepartStructure() {
		return currentRepartStructure;
	}

	public void setCurrentRepartStructure(EORepartStructure currentRepartStructure) {
		this.currentRepartStructure = currentRepartStructure;
	}
}