/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.PersonneGroupesForm;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class ContactsEntreprisesForm extends PersonneGroupesForm {
	
	private static final long serialVersionUID = 19L;
	
//	public static final String BINDING_editingContext = "editingContext";
	public static final String BINDING_personne = "personne";
//	public static final String BINDING_selectedRepartStructure = "selectedRepartStructure";
	public static final String BINDING_isEditing = "isEditing";
	public final static String BINDING_wantRefresh = "wantRefresh";
	private static final String ASS_CONTACTS = "CONTACT";
	
	
	private EOEditingContext _editingContext;
	private IPersonne _personne;
	
	private EORepartStructure unRepartStructure;
	private NSArray lesRepartStructures;
	private EORepartStructure editRepartStructure;
	private EORepartStructure newRepartStructure;
	private EORepartStructure repartStructure;
	
	private EOStructure selectedEntreprise;
	
	
    public ContactsEntreprisesForm(WOContext context) {
        super(context);
    }
    public IPersonne getPersonne() {
		IPersonne pers = (IPersonne) valueForBinding(BINDING_personne);
		if (pers == null) {
			_personne = null;
			lesRepartStructures = null;
		}
		else if (_personne == null || _personne.globalID() == null || !_personne.globalID().equals(pers.globalID())) {
			
			_personne = pers.localInstanceIn(edc());
		}
		return _personne;
	}
    
    
    public String entrepriseViewContainerId() {
		return getComponentId() + "_EntrepriseViewContainer";
	}
    
    public String getLibelleForEnterprise() {
    	if (unRepartStructure.toStructureGroupe() == null) {
			return null;
		}
		return MyStringCtrl.compactString(unRepartStructure.toStructureGroupe().getNomCompletAffichage(), 40, "...");
	}
    
    
    public Boolean hasElements() {
		return (lesRepartStructures() != null && lesRepartStructures().count() > 0);
	}
    
    public Boolean isEditing() {
		return (Boolean) valueForBinding(BINDING_isEditing);
	}
    
    public void setIsEditing(Boolean value) {
		setValueForBinding(value, BINDING_isEditing);
	}
    
    public NSArray lesRepartStructures() {
		if (getPersonne() != null) {
			// FIXME Le tri suivant provoque la premiere fois le fetch sur toPersonne pour chaque element
			EOQualifier qualifierDesEntreprises = EORepartStructure.TO_STRUCTURE_GROUPE.dot(EOStructure.TO_REPART_TYPE_GROUPES).dot(EORepartTypeGroupe.TGRP_CODE).containsObject("EN");
			lesRepartStructures = getPersonne().toRepartStructures(qualifierDesEntreprises);
			lesRepartStructures = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesRepartStructures, new NSArray(EOSortOrdering.sortOrderingWithKey(EORepartStructure.TO_STRUCTURE_ELTS_KEY + "." + IPersonne.NOM_PRENOM_AFFICHAGE_KEY, EOSortOrdering.CompareAscending)));
			return lesRepartStructures;
		}
		return NSArray.EmptyArray;
	}
    
    public EORepartStructure getRepartStructure() {
		return repartStructure;
	}

	public void setRepartStructure(EORepartStructure repartStructure) {
		this.repartStructure = repartStructure;
	}
    
	public EORepartStructure getNewRepartStructure() {
		return newRepartStructure;
	}

	public void setNewRepartStructure(EORepartStructure newRepartStructure) {
		this.newRepartStructure = newRepartStructure;
	}

	public EORepartStructure getUnRepartStructure() {
		return unRepartStructure;
	}

	public void setUnRepartStructure(EORepartStructure unRepartStructure) {
		this.unRepartStructure = unRepartStructure;
	}

	public WOActionResults onEntrepriseAnnuler() {
		
		edc().revert();
		setSelectedEntreprise(null);
		setIsEditing(Boolean.FALSE);
		return doNothing();
	}
    
    public WOActionResults onEntrepriseCreer() {

		setIsEditing(Boolean.TRUE);
		return doNothing();
	}
    
    public WOActionResults onEntrepriseEnregistrer() {
    	
    	if(getSelectedEntreprise() == null) {
    		mySession().addSimpleErrorMessage("Attention", "Vous devez sélectionner une entreprise");
    		return doNothing();
    	}
    	
    	NSArray<EOStructure> lesEntreprises = (NSArray<EOStructure>) lesRepartStructures().valueForKey(EORepartStructure.TO_STRUCTURE_GROUPE_KEY);
    	if(lesEntreprises.contains(getSelectedEntreprise())) {
    		mySession().addSimpleInfoMessage("Attention", "L'entreprise sélectionnée est déjà dans la liste");
    		return doNothing();
    	}
    	
		EOAssociation associationContacts;
        EOQualifier qual = ERXQ.equals(EOAssociation.ASS_LIBELLE_KEY, ASS_CONTACTS);
        associationContacts = EOAssociation.fetchFirstByQualifier(edc(), qual);
    	
		EOStructureForGroupeSpec.definitUnRole(
				edc(), 
				getPersonne(), 
				associationContacts, 
				getSelectedEntreprise(), 
				getUtilisateurPersId(),
				null, null, null, null, null, false);
		try {
			edc().saveChanges();
			setIsEditing(Boolean.FALSE);
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edc().revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		lesRepartStructures();
		return doNothing();
	}
    

	public WOActionResults onEntrepriseSupprimer() {
		EOEditingContext edForGroupe = edc();
		
		EOAssociation associationContacts;
        EOQualifier qual = ERXQ.equals(EOAssociation.ASS_LIBELLE_KEY, ASS_CONTACTS);
        associationContacts = EOAssociation.fetchFirstByQualifier(edc(), qual);
		
		getRepartStructure().deleteAllToRepartAssociationsRelationships();
		getRepartStructure().delete();
		try {
			edc().saveChanges();
			setRepartStructure(null);
			setIsEditing(Boolean.FALSE);
		} catch (NSValidation.ValidationException e) {
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		} catch (Exception e) {
			edForGroupe.revert();
			e.printStackTrace();
			setErreurSaisieMessage(e.getMessage());
		}
		lesRepartStructures();
		return null;
	}
	
    public Boolean isEntrepriseCreerEnabled() {
		//verifier que l'utilisateur a le droit de creer une nouvelle affectation au groupe ANNUAIRE_ENTREPRISE
		//		return Boolean.TRUE;
		return getAppUserForAnnuaire().hasDroitGererAuMoinsUnGroupe();
	}
    
    
    public Boolean isEntrepriseSupprimerEnabled() {
    	return Boolean.valueOf(getAppUserForAnnuaire().hasDroitModificationIPersonne(getPersonne()) && getRepartStructure() != null);
	} 
    
    
    public Boolean isCreatingNewLinkedEnterprise() {
		return Boolean.valueOf(edc().insertedObjects().indexOf(editRepartStructure) != NSArray.NotFound);
	}
    
	public EORepartStructure getEditRepartStructure() {
		return editRepartStructure;
	}

	public void setEditRepartStructure(EORepartStructure editRepartStructure) {
		this.editRepartStructure = editRepartStructure;
	}
	
	public String entrepriseGroupeContainerId() {
		return getComponentId() + "_" + "entrepriseGroupeContainer";
	}
	public String entrepriseFieldId() {
		return getComponentId() + "_" + "entrepriseField";
	}
	    
	public EOStructure getSelectedEntreprise() {
		return selectedEntreprise;
	}
	
	public void setSelectedEntreprise(EOStructure newEntreprise) {
		selectedEntreprise = newEntreprise;
	}

	public Boolean isNew() {
		return isCreatingNewLinkedEnterprise();
	}

	public EOQualifier getEntreprisesForIndividuQualifier() {
		String cStructure = EOGrhumParametres.parametrePourCle(edc(), "ANNUAIRE_ENTREPRISE");
		return EOStructure.C_STRUCTURE.eq(cStructure);
	}
	
}