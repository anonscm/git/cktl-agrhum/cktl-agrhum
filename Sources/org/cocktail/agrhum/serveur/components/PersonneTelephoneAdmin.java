/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

public class PersonneTelephoneAdmin extends MyWOComponent {
	private static final long serialVersionUID = -2778385285521872230L;
	
	public static final String BDG_editingContext = "editingContext";
	public static final String BDG_utilisateurPersId = "utilisateurPersId";
	public static final String BDG_personne = "personne";
	
	private EOQualifier qualifierTelPro;

	public PersonneTelephoneAdmin(WOContext context) {
        super(context);
    }
	
	public void setQualifierTelPro(){
		this.qualifierTelPro = EOTypeTel.QUAL_C_TYPE_TEL_PRF;
	}
	
	public EOQualifier qualifierTelPro() {
		if (this.qualifierTelPro == null) 
			this.qualifierTelPro = EOTypeTel.QUAL_C_TYPE_TEL_PRF;
		return this.qualifierTelPro;
	}
	
	/**
	 * @return the personne
	 */
	public IPersonne personne() {
		return (IPersonne)valueForBinding(BDG_personne);
	}

	/**
	 * @param personne the personne to set
	 */
	public void setPersonne(IPersonne personne) {
		setValueForBinding(personne, BDG_personne);
	}
	
}