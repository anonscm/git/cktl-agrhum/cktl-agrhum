/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXS;

public class SelectIndividusMembresGroupe extends MyWOComponent {
	
	private static final long serialVersionUID = 3607400336991142219L;
    private static final String EDITING_CONTEXT_BDG = "editingContext";
    private static final String STRUCTURE_BDG = "structure";

    private ERXDisplayGroup<IPersonne> selectedPersonnesDisplayGroup = new ERXDisplayGroup<IPersonne>();
    private ERXDisplayGroup<EORepartStructure> membresDisplayGroup = new ERXDisplayGroup<EORepartStructure>();
    private EOStructure structure;
    
    private Boolean selectedPersonneHasChanged = false;

	protected String reportFilename;

	
    public SelectIndividusMembresGroupe(WOContext context) {
        super(context);
        membresDisplayGroup.setDelegate(new MembresDisplayGroupDelegate());
    }

	public class MembresDisplayGroupDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedPersonneHasChanged(true);
		}
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
        updateDisplayGroup();
        super.appendToResponse(response, context);
    }
    
	public boolean hasMembres() {
        return getMembresDisplayGroup().allObjects().count() > 0;
    }
    
    public String membresContId() {
        return "MemCont_" + uniqueDomId();
    }
    
    public EOEditingContext editingContext() {
        return (EOEditingContext) valueForBinding(EDITING_CONTEXT_BDG);
    }
    
    public String ajouterPersonnesLabel() {
        return "Ajouter ces personnes au groupe " + structure().libelle();
    }
    
    public String selectionLabel() {
        if (getMembresDisplayGroup().selectedObjects().count() == 1) {
            return getMembresDisplayGroup().selectedObject().toPersonneElt().getNomPrenomAffichage();
        } else {
            return getMembresDisplayGroup().selectedObjects().count() + " personnes sélectionnées";
        }
    }
    
    public ERXDisplayGroup<IPersonne> selectedPersonnesDisplayGroup() {
        return selectedPersonnesDisplayGroup;
    }
    
    public ERXDisplayGroup<EORepartStructure> getMembresDisplayGroup() {
        return membresDisplayGroup;
    }
    
    public EOStructure structure() {
        EOStructure structureTmp = (EOStructure)valueForBinding(STRUCTURE_BDG);
        if (!ERXEOControlUtilities.eoEquals(structure, structureTmp)) {
            structure = structureTmp;
        }
        return structure;
    }
    
    public boolean auMoinsUnMembreSelectionne() {
        return getMembresDisplayGroup().selectedObjects().count() >= 1;
    }
    
    public void updateDisplayGroup() {
        getMembresDisplayGroup().setSelectsFirstObjectAfterFetch(false);
        /**
         * Le fetch n'est fait dans le Trombinoscope que pour les individus qui sont membres du groupe sélectionné.
         * Les sous-structures ont été pour l'instant volontairement éliminées du fetch.
         */
        NSArray<EORepartStructure> tampon = EORepartStructure.getRepartStructureWithIndividusValidesMembresDe(editingContext(), structure());
        tampon = EOSortOrdering.sortedArrayUsingKeyOrderArray(tampon, ERXS.ascInsensitives(ViewMembresGroupe.LIBELLE_STRUCTURE_KP));
        getMembresDisplayGroup().setObjectArray(tampon);
//        getMembresDisplayGroup().setObjectArray(
//        		EORepartStructure.getRepartStructureWithIndividusValidesMembresDe(editingContext(), structure()));
//        getMembresDisplayGroup().setObjectArray(getMembresDisplayGroup().sortedObjects());
//        getMembresDisplayGroup().setObjectArray(structure().toRepartStructuresElts(
//        		ERXQ.is(EORepartStructure.TO_PERSONNE_ELT_KEY + "." + IPersonne.IS_INDIVIDU_KEY, true), ERXS.ascInsensitives(ViewMembresGroupe.LIBELLE_STRUCTURE_KP), false));
        getMembresDisplayGroup().setSelectedObjects(NSArray.EmptyArray);
    }
    
    public String membresDetailId() {
        return "MemDetails_" + uniqueDomId();
    }
    
    public String trombiPreviewDialogId() {
        return "TrombiPreview_" + uniqueDomId();
    }
    
	public Boolean getSelectedPersonneHasChanged() {
		return selectedPersonneHasChanged;
	}

	public void setSelectedPersonneHasChanged(Boolean selectedPersonneHasChanged) {
		this.selectedPersonneHasChanged = selectedPersonneHasChanged;
	}
	
	public WOActionResults openTrombiPreview(){
		return null;
	}
	
	public WOActionResults onRecommencer() {
	  getMembresDisplayGroup().clearSelection();
	  AjaxUpdateContainer.updateContainerWithID(membresContId(), context());
	  return null;
	}
	
	public NSArray<EORepartStructure> listeSelection(){
		return getMembresDisplayGroup().selectedObjects();
	}
	
}