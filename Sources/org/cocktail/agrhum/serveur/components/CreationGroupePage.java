/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.agrhum.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSValidation;

/**
 * Page dédiée à la création d'un groupe, destiné à être affiché dans une 
 * fenêtre modale.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public class CreationGroupePage extends MyWOComponent {
    
    private static final long serialVersionUID = -2963417008265203001L;
    private EOStructure selectedStructureForCreation;
    private EOStructure parentStructure;
    private boolean resetCreation;
    private boolean isCreationMode;
    
    public CreationGroupePage(WOContext context) {
        super(context);
    }
    
    public WOActionResults onSelectStructureForCreation() {
        setResetCreation(true);
        setCreationMode(true);
        if (selectedStructureForCreation !=  null) {
            selectedStructureForCreation.registerSpecificite(EOStructureForGroupeSpec.sharedInstance());
            if (parentStructure != null)
                selectedStructureForCreation.initAvecParent(parentStructure);
        }
        return null;
    }
    
    public WOActionResults terminerCreation() {
        save();
        setResetCreation(true);
        CktlAjaxWindow.close(context());
        return null;
    }
    
    public WOActionResults annulerCreationAndClose() {
        CktlAjaxWindow.close(context());
        return null;
    }
    
    private void save() {
        try {
            edc().saveChanges();
        } catch (NSValidation.ValidationException e) {
            logger().info(e.getMessage(), e);
            session().addSimpleErrorMessage(e.getLocalizedMessage(), e);
            context().response().setStatus(500);
        } catch (Exception e) {
            logger().warn(e.getMessage(), e);
            context().response().setStatus(500);
            session().addSimpleErrorMessage(e.getLocalizedMessage(), e);
        }
    }
    
    public EOStructure getSelectedStructureForCreation() {
        return selectedStructureForCreation;
    }
    
    public void setSelectedStructureForCreation(EOStructure selectedStructureForCreation) {
        this.selectedStructureForCreation = selectedStructureForCreation;
    }
    
    public void setParentStructure(EOStructure parentStructure) {
        this.parentStructure = parentStructure;
    }
    
    public boolean isResetCreation() {
        return resetCreation;
    }

    public void setResetCreation(boolean resetCreation) {
        this.resetCreation = resetCreation;
        if (resetCreation)
            setCreationMode(false);
    }
    
    public void setCreationMode(boolean isCreationMode) {
        this.isCreationMode = isCreationMode;
    }
    
    public boolean isCreationMode() {
        return isCreationMode;
    }
    
}