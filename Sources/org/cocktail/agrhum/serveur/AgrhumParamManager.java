package org.cocktail.agrhum.serveur;

import org.cocktail.fwkcktldroitsutils.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.foundation.ERXThreadStorage;

public class AgrhumParamManager extends CktlParamManager {
	
	public static final String AGRHUM_CHECK_COHERENCE_INSEE_DISABLED = "org.cocktail.agrhum.individu.checkcoherenceinsee.disabled";
	public static final String AGRHUM_CHECK_MANGUE_INSTALLED = "org.cocktail.agrhum.grhumparametres.checkmangue.used";
//	public static final String AGRHUM_CHECK_MANGUE_INSTALLED = "GRHUM_RH";
	public static final String AGRHUM_CHECK_SCOLARIX_INSTALLED = "org.cocktail.agrhum.grhumparametres.checkscolarix.used";
//	public static final String AGRHUM_CHECK_SCOLARIX_INSTALLED = "GRHUM_PEDA";
	public static final String AGRHUM_ADRESSE_PERSO_DESACTIVE = "org.cocktail.agrhum.adresse.personaladressmodification.disabled";
	public static final String AGRHUM_PERSONNE_NOM_READONLY_ACTIVE = "org.cocktail.agrhum.personne.nom.readonly.enabled";
	
	public static final String AGRHUM_CPTE_STOCKAGE_CLAIR_ACTIVE = "org.cocktail.agrhum.compte.stockageenclair.enabled";
	public static final String AGRHUM_FOURNISSEUR_BT_MAIL = "org.cocktail.agrhum.fournisseur.btenvoimail.disabled";
	
	public static final String AGRHUM_PHOTO_READONLY_ACTIVE = "org.cocktail.agrhum.photo.readonly.enabled";
	
	public static final String AGRHUM_RESULTAT_RECHERCHE_LIMITE = "org.cocktail.agrhum.recherche.max";
	
	public static final String AGRHUM_EXPORT_PASSWORD = "org.cocktail.agrhum.export.password.enabled";
	public static final String AGRHUM_EXPORT_BIRTHDATE = "org.cocktail.agrhum.export.birthdate.enabled";
	
	public static final String AGRHUM_FOURNISSEUR_TYPE_DEFAUT = "org.cocktail.agrhum.fournisseur.typeDefaut";
	
	public static final String AGRHUM_FOURNISSEUR_ACCES_STATUT_FONCTION = "org.cocktail.agrhum.fournisseur.acces.statutfonction.enabled";

	private EOEditingContext ec = ERXEC.newEditingContext();

	public AgrhumParamManager() {
		getParamList().add(AGRHUM_CHECK_COHERENCE_INSEE_DISABLED);
		getParamComments().put(AGRHUM_CHECK_COHERENCE_INSEE_DISABLED, "Désactiver ou non les tests de cohérence du numéro INSEE");
		getParamDefault().put(AGRHUM_CHECK_COHERENCE_INSEE_DISABLED, "NON");
		getParamTypes().put(AGRHUM_CHECK_COHERENCE_INSEE_DISABLED, EOGrhumParametresType.codeActivation);
	
		
		getParamList().add(AGRHUM_CHECK_MANGUE_INSTALLED);
		getParamComments().put(AGRHUM_CHECK_MANGUE_INSTALLED, "Souhaitez-vous utiliser Mangue pour gérer l'état civil des agents (désactivation dans AGrhum) ? (OUI / NON)");
		getParamDefault().put(AGRHUM_CHECK_MANGUE_INSTALLED, "NON");
		getParamTypes().put(AGRHUM_CHECK_MANGUE_INSTALLED, EOGrhumParametresType.codeActivation);
		
		getParamList().add(AGRHUM_CHECK_SCOLARIX_INSTALLED);
		getParamComments().put(AGRHUM_CHECK_SCOLARIX_INSTALLED, "Souhaitez-vous utiliser l'application de scolarité pour gérer l'état civil des étudiants (désactivation dans AGrhum) ? (OUI / NON)");
		getParamDefault().put(AGRHUM_CHECK_SCOLARIX_INSTALLED, "NON");
		getParamTypes().put(AGRHUM_CHECK_SCOLARIX_INSTALLED, EOGrhumParametresType.codeActivation);
		
		
		getParamList().add(AGRHUM_ADRESSE_PERSO_DESACTIVE);
		getParamComments().put(AGRHUM_ADRESSE_PERSO_DESACTIVE, "Autoriser ou non un individu à modifier son adresse personnelle dans Agrhum");
		getParamDefault().put(AGRHUM_ADRESSE_PERSO_DESACTIVE, "N");
		getParamTypes().put(AGRHUM_ADRESSE_PERSO_DESACTIVE, EOGrhumParametresType.codeActivation);

		getParamList().add(AGRHUM_PERSONNE_NOM_READONLY_ACTIVE);
		getParamComments().put(AGRHUM_PERSONNE_NOM_READONLY_ACTIVE, "Mettre le nom des personnes en lecture seulement si utilisateur pas GrhumCreateur");
		getParamDefault().put(AGRHUM_PERSONNE_NOM_READONLY_ACTIVE, "N");
		getParamTypes().put(AGRHUM_PERSONNE_NOM_READONLY_ACTIVE, EOGrhumParametresType.codeActivation);
		
		getParamList().add(AGRHUM_CPTE_STOCKAGE_CLAIR_ACTIVE);
		getParamComments().put(AGRHUM_CPTE_STOCKAGE_CLAIR_ACTIVE, "Activer ou désactiver le stockage en clair du mot de passe (Défaut : N)");
		getParamDefault().put(AGRHUM_CPTE_STOCKAGE_CLAIR_ACTIVE, "N");
		getParamTypes().put(AGRHUM_CPTE_STOCKAGE_CLAIR_ACTIVE, EOGrhumParametresType.codeActivation);

		getParamList().add(AGRHUM_FOURNISSEUR_BT_MAIL);
		getParamComments().put(AGRHUM_FOURNISSEUR_BT_MAIL, "Activer ou désactiver le bouton d'envoi d'email d'info à un fournisseur (Défaut : N)");
		getParamDefault().put(AGRHUM_FOURNISSEUR_BT_MAIL, "N");
		getParamTypes().put(AGRHUM_FOURNISSEUR_BT_MAIL, EOGrhumParametresType.codeActivation);

		getParamList().add(AGRHUM_PHOTO_READONLY_ACTIVE);
		getParamComments().put(AGRHUM_PHOTO_READONLY_ACTIVE, "Mettre ou non la photo d'un individu en lecture seulement");
		getParamDefault().put(AGRHUM_PHOTO_READONLY_ACTIVE, "N");
		getParamTypes().put(AGRHUM_PHOTO_READONLY_ACTIVE, EOGrhumParametresType.codeActivation);


		getParamList().add(AGRHUM_RESULTAT_RECHERCHE_LIMITE);
		getParamComments().put(AGRHUM_RESULTAT_RECHERCHE_LIMITE, "Nombre maximal de recherche effectuée dans l'application Profiler (Défaut : 40).");
		getParamDefault().put(AGRHUM_RESULTAT_RECHERCHE_LIMITE, "40");
		getParamTypes().put(AGRHUM_RESULTAT_RECHERCHE_LIMITE, EOGrhumParametresType.valeurNumerique);
		
		
		getParamList().add(AGRHUM_EXPORT_PASSWORD);
		getParamComments().put(AGRHUM_EXPORT_PASSWORD, "Activer ou désactiver l'export du mot de passe (Défaut : N)");
		getParamDefault().put(AGRHUM_EXPORT_PASSWORD, "N");
		getParamTypes().put(AGRHUM_EXPORT_PASSWORD, EOGrhumParametresType.codeActivation);
		
		getParamList().add(AGRHUM_EXPORT_BIRTHDATE);
		getParamComments().put(AGRHUM_EXPORT_BIRTHDATE, "Activer ou désactiver l'export de la date de naissance de l'individu (Défaut : N)");
		getParamDefault().put(AGRHUM_EXPORT_BIRTHDATE, "N");
		getParamTypes().put(AGRHUM_EXPORT_BIRTHDATE, EOGrhumParametresType.codeActivation);
		
		
		
		getParamList().add(AGRHUM_FOURNISSEUR_TYPE_DEFAUT);
		getParamComments().put(AGRHUM_FOURNISSEUR_TYPE_DEFAUT, "Type de fournisseur à préselectionner lors de la création des fournisseurs (F: fournisseur, C: Client, T: Tiers)");
		getParamDefault().put(AGRHUM_FOURNISSEUR_TYPE_DEFAUT, EOFournis.FOU_TYPE_FOURNISSEUR);
		getParamTypes().put(AGRHUM_FOURNISSEUR_TYPE_DEFAUT, EOGrhumParametresType.texteLibre);
		

		getParamList().add(AGRHUM_FOURNISSEUR_ACCES_STATUT_FONCTION);
		getParamComments().put(AGRHUM_FOURNISSEUR_ACCES_STATUT_FONCTION, "Accès en écriture pour les fournisseurs individus au statut et à la fonction (Défaut : N)");
		getParamDefault().put(AGRHUM_FOURNISSEUR_ACCES_STATUT_FONCTION, "N");
		getParamTypes().put(AGRHUM_FOURNISSEUR_ACCES_STATUT_FONCTION, EOGrhumParametresType.codeActivation);
		
	}
	
	@Override
	public void createNewParam(String key, String value, String comment) {
		createNewParam(key, value, comment, EOGrhumParametresType.codeActivation);
	}
	
	@Override
	public void checkAndInitParamsWithDefault() {
		//Recuperer un grhum_createur
		String cptLogin = EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_CREATEUR);
		if (cptLogin != null) {
			EOCompte cpt = EOCompte.compteForLogin(ec, cptLogin);
			if (cpt != null) {
				ERXThreadStorage.takeValueForKey(cpt.persId(), PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
			}
		}
		super.checkAndInitParamsWithDefault();
	}

	@Override
	public void createNewParam(String key, String value, String comment,
			String type) {
		EOGrhumParametres newParametre = EOGrhumParametres.creerInstance(ec);
		newParametre.setParamKey(key);
		newParametre.setParamValue(value);
		newParametre.setParamCommentaires(comment);
		newParametre.setToParametresTypeRelationship(EOGrhumParametresType.fetchByKeyValue(ec, EOGrhumParametresType.TYPE_ID_INTERNE_KEY, type));
		if (ec.hasChanges()) {
			EOEntity entityParameter = ERXEOAccessUtilities.entityForEo(newParametre);
			try {

				// Avant de sauvegarder les données, nous modifions le modèle
				// pour que l'on puisse avoir accès aussi en écriture sur les données
				entityParameter.setReadOnly(false);
				ec.saveChanges();

			} catch (Exception e) {
				log.warn("Erreur lors de l'enregistrement des parametres.");
				e.printStackTrace();
			} finally {
				entityParameter.setReadOnly(true);
			}
		}
	}

	@Override
	public String getParam(String key) {
		String res = getApplication().config().stringForKey(key);
		return res;
	}


}
