package org.cocktail.agrhum.serveur.metier;

import org.apache.log4j.Logger;
import org.cocktail.agrhum.serveur.Session;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;

/**
 * définit le service appelé lors de la sauvegarde des modification dans l'éditing contexte
 */
public interface SauvegardeService {

	/**
	 * sauvegarde les informations contenues dans l'editing context
	 * @param edc editing context qui contien les changements à apporter en base
	 * @param session session dans laquelle se trouve l'application
	 * @param logger permet de logger les messages
	 * @param context contexte de la session
	 */
	void save(EOEditingContext edc, EOEnterpriseObject objet, boolean booleanValue);
}
