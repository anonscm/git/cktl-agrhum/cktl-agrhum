package org.cocktail.agrhum.serveur.metier;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.server.util.CompteService;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

/**
 * sauvegarde une personne modifiée dans l'editing context
 */
public class PersonneSauvegardeService implements SauvegardeService {


	/**
	 * {@inheritDoc}
	 */
	public void save(EOEditingContext edc, EOEnterpriseObject objet, boolean booleanValue) {
		
//		boolean isIndividu = false;
//		IPersonne personne = (IPersonne) objet;
//		
//		if (personne instanceof IIndividu) {
//			isIndividu = personne.isIndividu();
//		}
		
		CompteService compteService = new CompteService();
		CktlDataBus databus = ((CktlWebApplication) CktlWebApplication.application()).dataBus();
		
		switchingTriggers(edc, booleanValue, compteService, databus);

	}

	protected void switchingTriggers(EOEditingContext edc,
			boolean booleanValue, CompteService compteService,
			CktlDataBus databus) {
		String exp;
		
		exp = "ALTER TRIGGER GRHUM.TRG_BR_INDIVIDU_ULR DISABLE";
		databus.executeSQLQuery(exp, NSArray.EmptyArray);
		
		exp = "ALTER TRIGGER GRHUM.TRG_COMPTE DISABLE";
		databus.executeSQLQuery(exp, NSArray.EmptyArray);
		
		exp = "ALTER TRIGGER GRHUM.TRG_REPART_COMPTE DISABLE";
		databus.executeSQLQuery(exp, NSArray.EmptyArray);
		
		edc.saveChanges();
		
	}

}
