package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

import com.webobjects.eocontrol.EOEditingContext;

public interface IExportInformations {

	
	public String get(EOIndividu individu);
	
	public String get(EOStructure structure);
	
	public EOEditingContext getContext();

	public void setContext(EOEditingContext editing);
}
