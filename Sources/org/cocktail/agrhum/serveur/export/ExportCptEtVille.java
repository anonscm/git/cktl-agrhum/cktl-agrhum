package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

public class ExportCptEtVille extends AbstractExportInformation implements IExportInformations{

	
	public String get(EOIndividu individu) {
		return getCpEtVille(individu, "PRO");
	}
	
	
	public String get(EOStructure structure) {
		return getCpEtVille(structure, "PRO");
	}
	
	private String getCpEtVille(IPersonne personne, String typeAdresse){
		EOAdresse adresse = getAdresseTypee(personne, typeAdresse);
		if (adresse == null) {
			return noString();
		}
		String cpEtVille = adresse.codePostal() + " " + adresse.ville();
		return cpEtVille;
	}
	
	
	
	
	

}
