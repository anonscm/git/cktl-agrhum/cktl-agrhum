package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

public class exportAdresseFacturation extends AbstractExportInformation implements IExportInformations{

	
	public String get(EOIndividu individu) {
		return getAdresseFacturation(individu);
	}
	
	public String get(EOStructure structure) {
		return noString();
	}
	
	private String getAdresseFacturation(IPersonne personne){
		
		String adresseFacturation;
		EOAdresse adresse = getAdresseTypee(personne, "FACT");
		if (adresse == null) {
			return noString();
		}
		
		if (adresse.adrAdresse1() != null && !adresse.adrAdresse1().equals("") && !adresse.adrAdresse1().equals(" ")) {
			adresseFacturation = adresse.adrAdresse1();
		} else {
			adresseFacturation = noString();
		}

		if (adresse.adrAdresse2() != null && !adresse.adrAdresse2().equals("") && !adresse.adrAdresse2().equals(" ")) {
			adresseFacturation += " - " + adresse.adrAdresse2();
		}
		if (adresse.adrAdresse3() != null && !adresse.adrAdresse3().equals("") && !adresse.adrAdresse3().equals(" ")) {
			adresseFacturation += " - " + adresse.adrAdresse3();
		}
		if (adresse.adrAdresse4() != null && !adresse.adrAdresse4().equals("") && !adresse.adrAdresse4().equals(" ")) {
			adresseFacturation += " - " + adresse.adrAdresse4();
		}
		if (adresse.adrBp() != null && !adresse.adrBp().equals("") && !adresse.adrBp().equals(" ")) {
			adresseFacturation += " - " + adresse.adrBp();
		}
		if (adresse.codePostal() != null &&  adresse.ville() != null) {
			adresseFacturation += "  " + adresse.codePostal() + " " + adresse.ville();
		}
		
		return adresseFacturation;
	}
}
