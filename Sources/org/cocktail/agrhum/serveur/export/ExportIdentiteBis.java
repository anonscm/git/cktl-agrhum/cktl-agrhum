package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

public class ExportIdentiteBis extends AbstractExportInformation implements IExportInformations{

	public String get(EOIndividu individu) {
		String identiteBis = individu.toCivilite().cCivilite() + " ";
		identiteBis += individu.prenomAffichage() + " " + individu.nomAffichage();
		return identiteBis;
	}
	
	/**
	 * Retourne l'identité de la structure.
	 */
	public String get(EOStructure structure) {
		String identite = structure.getNomCompletAffichage();
		return identite;
	}
}
