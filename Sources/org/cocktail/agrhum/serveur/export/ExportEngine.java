package org.cocktail.agrhum.serveur.export;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriere;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.server.util.CompteService;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import static org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl.*;

public class ExportEngine {

	private HashMap<String, IExportInformations> exportsIndividu = new HashMap<String, IExportInformations>();
	
	private CompteService compteService = new CompteService();
	private EOEditingContext editingContext;
	
	
	
	public ExportEngine() {
		exportsIndividu.put(INFO_IDENTITE_CIV_NOM_PRENOM, new ExportIdentite());
		exportsIndividu.put(INFO_IDENTITE_CIV_PRENOM_NOM, new ExportIdentiteBis());
		exportsIndividu.put(INFO_ADRESSE_BP_PRO, new ExportAdresseEtBp("PRO"));
		exportsIndividu.put(INFO_ADRESSE_BP_PERSO, new ExportAdresseEtBp("PERSO"));
		exportsIndividu.put(INFO_FONCTION_QUALITE, new ExportFonctionQualite());
		exportsIndividu.put(INFO_EMAIL, new ExportMail());
		exportsIndividu.put(INFO_ACTIVITE, new ExportActivite());
		exportsIndividu.put(INFO_EMAIL_PERSO, new ExportMailPerso());
		exportsIndividu.put(INFO_LOGIN, new ExportLogin());
		exportsIndividu.put(INFO_PASSWORD, new ExportMdp());
		exportsIndividu.put(INFO_EMAIL_PERSO, new ExportMailPerso());
		exportsIndividu.put(INFO_CP_VILLE_PRO, new ExportCptEtVille());
		exportsIndividu.put(INFO_FAX_PRO, new ExportFaxPro());
		exportsIndividu.put(INFO_TEL_PRO, new ExportTelType());
		exportsIndividu.put(INFO_TEL_PERSO, new ExportTelTypePerso());
		exportsIndividu.put(INFO_ADRESSE_FACTURATION, new exportAdresseFacturation());
		exportsIndividu.put(INFO_SERVICE, new ExportInfoService());
		exportsIndividu.put(INFO_POLE, new ExportInfoPoleComposante());
		exportsIndividu.put(INFO_CORPS, new ExportInfoCorps());
		exportsIndividu.put(INFO_FONCTION_INDIVIDU, new ExportInfoFonctionIndividu());
		exportsIndividu.put(INFO_BIRTH_DATE, new ExportBirthDate());
	}
	
	protected void exportMembre(NSArray<String> infosSelectionne,
			CktlXMLWriter w, String racineXML, IPersonne personneMembre,
			Integer index) throws IOException {
		String composant;
		for (String info : infosSelectionne) {
			composant = racineXML + index.toString();
			
			String donneeInfo = extraireUneInfo(personneMembre, info);
			
			w.writeElement(composant, donneeInfo);
			index++;
		}
	}

	protected String extraireUneInfo(IPersonne personneMembre, String info) {
		String donneeInfo;
		
		if (personneMembre.isIndividu()) {
			donneeInfo = getInfoPerso(info, (EOIndividu) personneMembre, " ");
		} else {
			donneeInfo = getInfoPerso(info, (EOStructure) personneMembre, " ");
		}
		return donneeInfo;
	}
	
	/**
	 * Retourne l'information demandée pour une structure .
	 * 
	 * @param intitule Le code de l'information à récupérer
	 * @param structure La structure
	 * @param valeurParDefaut La valeur retournée si la structure n'a pas cette information renseignée
	 * @return L'information demandée
	 */
	public String getInfoPerso(String intitule, EOStructure structure, String valeurParDefaut) {
		
		String donnee = noString();
		
		
		if (exportsIndividu.containsKey(intitule)) {
			IExportInformations export = exportsIndividu.get(intitule);
			export.setContext(editingContext());
			donnee =  export.get(structure);
			
		} else {
			donnee = valeurParDefaut;
		}
		
		return donnee;
	}
	
	
	
	// Renvoi des lignes pour le fichier XML
	public String getInfoPerso(String intitule, EOIndividu individu, String valeurParDefaut) {
		
		String donnee = noString();
		
		if (exportsIndividu.containsKey(intitule)) {
			IExportInformations export = exportsIndividu.get(intitule);
			export.setContext(editingContext());
			donnee =  export.get(individu);
			
		} else {
			donnee = valeurParDefaut;
		}
		
		return donnee;
	}
	
	
	
	public String noString(){
		return "---";
	}

	
	public StringWriter creerXml(EOStructure groupe, NSArray<EORepartStructure> elementsSelectionnes, NSArray<String> infosSelectionne) throws Exception {

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);
		
		String racineXML = "champsN";
		String composant;

		w.startDocument();
		w.startElement("groupe");


		for (EORepartStructure membre : elementsSelectionnes) {
			
			IPersonne personneMembre = membre.toPersonneElt();
			
			w.startElement("membre");
			
			Integer index = 1;
			
			exportMembre(infosSelectionne, w, racineXML, personneMembre, index);
			w.endElement();
			
		}

		w.endElement();
		w.endDocument();
		w.close();

		return sw;

	}
	
	public void setCompteService(CompteService compteService) {
        this.compteService = compteService;
    }

	public EOEditingContext editingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
}
