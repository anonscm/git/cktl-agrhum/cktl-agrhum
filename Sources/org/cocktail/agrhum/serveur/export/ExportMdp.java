package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktldepense.client.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

public class ExportMdp extends AbstractExportInformation implements IExportInformations{

	// Renvoi le mot de passe associé au compte prioritaire
	public String get(
			org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu) {
		if (getComptePrioritaire((IPersonne)individu) != null) {
			String motDePasse = getComptePrioritaire((IPersonne)individu).cptPasswdClair();
			return motDePasse;
		} else {
			return noString();
		}
	}
	
	public String get(EOStructure structure) {
		return noString();
	}
}
