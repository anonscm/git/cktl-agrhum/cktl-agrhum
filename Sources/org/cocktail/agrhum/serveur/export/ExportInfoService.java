package org.cocktail.agrhum.serveur.export;

import static org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl.INFO_POLE_LIBELLE_COMPOSANTE;
import static org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl.INFO_SERVICE;

import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class ExportInfoService extends AbstractExportInformation implements IExportInformations{

	
	public String get(EOIndividu individu) {
		return getAffectation(individu, new NSTimestamp());
	}
	
	public String get(EOStructure structure) {
		return noString();
	}
	
	
	
	// Renvoi un dictionnaire avec l'affectation renseignée pour la carrière
	@SuppressWarnings("unchecked")
	public String getAffectation(EOIndividu individu, NSTimestamp dateAffect) {
		String result = noString();
		
		EOQualifier myQualifier;
		EOFetchSpecification myFetch;
		NSMutableDictionary<String, String> myMDico = new NSMutableDictionary<String, String>();
		NSArray<EOAffectation> localResult;
		
		@SuppressWarnings("rawtypes")
		NSMutableArray args = new NSMutableArray(individu.noIndividu());
		args.addObject(dateAffect);
		args.addObject(dateAffect);
		
		myQualifier = EOQualifier.qualifierWithQualifierFormat("noDossierPers=%@ and dDebAffectation<=%@ and (dFinAffectation>=%@ or dFinAffectation=nil)", args);
		// Modification du 24/02/15 pour prendre en compte le témoin de validité de l'affectation
		EOQualifier qualValide = new EOKeyValueQualifier(EOAffectation.TEM_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, EOAffectation.OUI);
		
		EOQualifier myQualifierValide = new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
				myQualifier, qualValide
		}));
		
//		myFetch = new EOFetchSpecification(EOAffectation.ENTITY_NAME, myQualifier, null);
		myFetch = new EOFetchSpecification(EOAffectation.ENTITY_NAME, myQualifierValide, null);
		
		localResult = getContext().objectsWithFetchSpecification(myFetch);
		if (localResult.count() > 0) {
			EOAffectation affectation = localResult.objectAtIndex(0);
			myMDico.setObjectForKey(affectation.toStructure().libelle(), INFO_SERVICE);
			EOStructure composante = EOStructure.getComposante(getContext(), affectation.toStructure().cStructure());
			if (composante != null) {
				myMDico.setObjectForKey(composante.libelle(), INFO_POLE_LIBELLE_COMPOSANTE);
			}
			result = myMDico.get(INFO_SERVICE);
		}
		
		return result;
	}
}
