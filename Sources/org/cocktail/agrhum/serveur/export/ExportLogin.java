package org.cocktail.agrhum.serveur.export;

import static org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl.INFO_LOGIN;

import org.cocktail.fwkcktldepense.client.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.server.util.CompteService;

import com.webobjects.foundation.NSArray;

public class ExportLogin extends AbstractExportInformation implements IExportInformations {

	private CompteService compteService = new CompteService();

	public String get(
			org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu) {
		if (getComptePrioritaire((IPersonne)individu) != null) {
			String login = getComptePrioritaire((IPersonne)individu).cptLogin();
			return login;
		} else {
			return noString();
		}
	}
	
	
	public String get(EOStructure structure) {
		return getLogin(structure);
	}
	
	// Renvoi le login associé au compte prioritaire
	private String getLogin(IPersonne personne){
		if (getComptePrioritaire(personne) != null) {
			String login = getComptePrioritaire(personne).cptLogin();
			return login;
		} else {
			return noString();
		}
		
	}
	
	public EOCompte getComptePrioritaire(IPersonne personne) {
		NSArray<EOCompte> arrayComptes = personne.toComptes(EOCompte.QUAL_CPT_VALIDE_OUI, true);
		if (arrayComptes != null && compteService.classerParPriorite(arrayComptes).count() != 0) {
			if (!arrayComptes.isEmpty() && arrayComptes.count() != 0) {
				EOCompte compte;
				if (arrayComptes.count() > 1) {
					compte = (EOCompte) compteService.classerParPriorite(arrayComptes).objectAtIndex(0);
				} else {
					compte = arrayComptes.objectAtIndex(0);
				}
				
				return compte;
			}
		}
		return null;
		
	}



}
