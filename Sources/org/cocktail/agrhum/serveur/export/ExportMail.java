package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.server.util.CompteService;

import com.webobjects.foundation.NSArray;

public class ExportMail extends AbstractExportInformation implements
		IExportInformations {

	private CompteService compteService = new CompteService();

	// Renvoi l'email professionnel (le premier trouvé d'après les priorités)
	public String get(EOIndividu individu) {
		if (getComptePrioritaire((IPersonne) individu) != null
				&& getComptePrioritaire((IPersonne) individu).toCompteEmail() != null) {
			String email = getComptePrioritaire((IPersonne) individu)
					.toCompteEmail().cemEmail();
			email += "@"
					+ getComptePrioritaire((IPersonne) individu)
							.toCompteEmail().cemDomaine();
			return email;
		} else {
			return noString();
		}

	}

	
	public String get(EOStructure structure) {
		return getEmail(structure);
	}
	
	
	
	// Renvoi l'email professionnel (le premier trouvé d'après les priorités)
	private String getEmail(IPersonne personne) {
		if (getComptePrioritaire(personne) != null && getComptePrioritaire(personne).toCompteEmail() != null) {
			String email = getComptePrioritaire(personne).toCompteEmail().cemEmail();
			email += "@" + getComptePrioritaire(personne).toCompteEmail().cemDomaine();
			return email;
		} else {
			return noString();
		}
		
	}
	
	
	public EOCompte getComptePrioritaire(IPersonne personne) {
		NSArray<EOCompte> arrayComptes = personne.toComptes(EOCompte.QUAL_CPT_VALIDE_OUI, true);

		if (arrayComptes != null && compteService.classerParPriorite(arrayComptes).count() != 0) {
			if (!arrayComptes.isEmpty() && arrayComptes.count() != 0) {
				EOCompte compte;
				if (arrayComptes.count() > 1) {
					compte = (EOCompte) compteService.classerParPriorite(arrayComptes).objectAtIndex(0);
				} else {
					compte = arrayComptes.objectAtIndex(0);
				}

				return compte;
			}
		}
		return null;
	}
}
