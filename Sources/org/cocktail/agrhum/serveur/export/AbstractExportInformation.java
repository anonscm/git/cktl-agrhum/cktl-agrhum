package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.server.util.CompteService;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class AbstractExportInformation {

	protected EOEditingContext context;
	protected CompteService compteService = new CompteService();
	private String param;
	

	public AbstractExportInformation() {
		super();
	}

	public EOEditingContext getContext() {
		return context;
	}

	public void setContext(EOEditingContext editing) {
		this.context = editing;
	}
	
	public String noString() {
		return "---";
	}
	
	public EOCompte getComptePrioritaire(IPersonne personne) {
		NSArray<EOCompte> arrayComptes = personne.toComptes(EOCompte.QUAL_CPT_VALIDE_OUI, true);
		if (arrayComptes != null && compteService.classerParPriorite(arrayComptes).count() != 0) {
//		if (!arrayComptes.isEmpty() && arrayComptes.count() != 0) {
			EOCompte compte;
			if (arrayComptes.count() > 1) {
				compte = (EOCompte) compteService.classerParPriorite(arrayComptes).objectAtIndex(0);
			} else {
				compte = arrayComptes.objectAtIndex(0);
			}
			
			return compte;
		}
		return null;
		
	}
	
	public EOAdresse getAdresseTypee(IPersonne personne, String typeAdresse) {
		NSArray<EORepartPersonneAdresse> repartPersonneAdresse = null;
		EOAdresse adresse;
		if (typeAdresse == null || typeAdresse.equals("") || typeAdresse.equals(" ")) {
			return  null;
		}
		if (typeAdresse.equals("PRO")) {
			repartPersonneAdresse = (NSArray<EORepartPersonneAdresse>) EORepartPersonneAdresse.adressesValidesDeType(getContext(), personne, "PRO");
		}
		if (typeAdresse.equals("PERSO")) {
			repartPersonneAdresse = (NSArray<EORepartPersonneAdresse>) EORepartPersonneAdresse.adressesPersoValides(getContext(), personne);
		}
		if (typeAdresse.equals("FACT")) {
			repartPersonneAdresse = (NSArray<EORepartPersonneAdresse>) EORepartPersonneAdresse.adressesValidesDeType(getContext(), personne, "FACT");
		}
		
		if (repartPersonneAdresse.count() > 0) {
			adresse = repartPersonneAdresse.objectAtIndex(0).toAdresse();
			return adresse;
			} else {
				return null;
			}
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}
	
}