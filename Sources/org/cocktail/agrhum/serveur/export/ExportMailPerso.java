package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.foundation.NSArray;

public class ExportMailPerso extends AbstractExportInformation implements IExportInformations{

	//Renvoi l'email personnel
	@SuppressWarnings("unchecked")
	public String get(EOIndividu individu) {
		NSArray<EORepartPersonneAdresse> repartPersonneAdresse = new NSArray<EORepartPersonneAdresse>();

		repartPersonneAdresse = (NSArray<EORepartPersonneAdresse>) EORepartPersonneAdresse.adressesPersoValides(getContext(), individu);
		
		if (repartPersonneAdresse == null) {
			return noString();
		}
		
		if (repartPersonneAdresse.count() > 0) {
			return repartPersonneAdresse.objectAtIndex(0).eMail();
		} else {
			return noString();
		}
	}
	
	public String get(EOStructure structure) {
		return noString();
	}
}
