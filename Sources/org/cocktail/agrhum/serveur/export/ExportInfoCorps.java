package org.cocktail.agrhum.serveur.export;

import static org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl.INFO_CORPS_CODE;
import static org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl.INFO_CORPS_LC;
import static org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl.INFO_CORPS_LL;

import org.cocktail.fwkcktlgrh.common.metier.EOCarriere;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class ExportInfoCorps extends AbstractExportInformation implements IExportInformations{

	
	public String get(EOIndividu individu) {
		return getCarriere(individu, new NSTimestamp());
	}
	
	public String get(EOStructure structure) {
		return noString();
	}
	
	
	
	// Renvoi un élément de carrière déterminé par une date
	public String getCarriere(EOIndividu individu, NSTimestamp dateCarr) {
		String result = noString();
		NSMutableDictionary<String, String> myMDico = new NSMutableDictionary<String, String>();
		NSArray<EOCarriere> localResult;
		localResult = EOCarriere.carrieresForIndividu(getContext(), individu);
		
		if (localResult.count() > 0) {
			EOElements eltCarriere = EOElements.elementCourant(getContext(), individu);
			if (eltCarriere != null) {
				myMDico.setObjectForKey(eltCarriere.toCorps().cCorps(), INFO_CORPS_CODE);
				myMDico.setObjectForKey(eltCarriere.toCorps().lcCorps(), INFO_CORPS_LC);
				myMDico.setObjectForKey(eltCarriere.toCorps().llCorps(), INFO_CORPS_LL);

				result =  myMDico.get(INFO_CORPS_LC);
			}
			
		}
		
		return result;
	}
}
