package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

public class ExportFonctionQualite extends AbstractExportInformation implements IExportInformations{

	public String get(EOIndividu individu){
		if (individu.indQualite() != null) {
			return individu.indQualite();
		} else {
			return noString();
		}
	}
	
	public String get(EOStructure structure) {
		return noString();
	}
}
