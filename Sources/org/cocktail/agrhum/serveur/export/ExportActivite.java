package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

public class ExportActivite extends AbstractExportInformation implements IExportInformations{

	// Renvoi l'activité ou le statut de l'individu sélectionné
	public String get(EOIndividu individu){
		if (individu.indActivite() != null) {
			return individu.indActivite();
		} else {
			return noString();
		}
	}
	
	
	public String get(EOStructure structure) {
		String identite = structure.getNomCompletAffichage();
		return identite;
	}
}
