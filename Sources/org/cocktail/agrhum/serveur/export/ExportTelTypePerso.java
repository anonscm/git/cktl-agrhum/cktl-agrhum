package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class ExportTelTypePerso extends AbstractExportInformation implements IExportInformations{

	
	
	public String get(EOIndividu individu) {
		return getTelType(individu, "PRV");
	}
	
	public String get(EOStructure structure) {
		return noString();
	}
	// Renvoi le numéro de téléphone professionnel ou le personnel en fonction du type
	private String getTelType(IPersonne personne, String typeTel){
		EOQualifier myQualifier = null;

		NSArray<EOPersonneTelephone> telephones;

		if (typeTel.equals("PRF")) {
			myQualifier = EOPersonneTelephone.PERS_ID.eq(personne.persId()).and(EOPersonneTelephone.TYPE_TEL.eq("PRF").and(EOPersonneTelephone.TYPE_NO.eq("TEL")));
		}
		if (typeTel.equals("PRV")) {
			myQualifier = EOPersonneTelephone.PERS_ID.eq(personne.persId()).and(EOPersonneTelephone.TYPE_TEL.eq("PRV").and(EOPersonneTelephone.TYPE_NO.eq("TEL")));
		}
		
		telephones = EOPersonneTelephone.fetchAll(getContext(), myQualifier);
		
		if (telephones.count() > 0) {
			return telephones.objectAtIndex(0).noTelephone();
		} else {
			return noString();
		}
	}
}
