package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class ExportFaxPro extends AbstractExportInformation implements IExportInformations{

	
	public String get(EOIndividu individu) {
		return getFaxPro(individu);
	}
	
	public String get(EOStructure structure) {
		return getFaxPro(structure);
	}
	
	public String getFaxPro(IPersonne personne) {
		EOQualifier myQualifier;
		NSArray<EOPersonneTelephone> telephones;
		
		
		myQualifier = EOPersonneTelephone.PERS_ID.eq(personne.persId()).and(EOPersonneTelephone.TYPE_TEL.eq("PRF").and(EOPersonneTelephone.TYPE_NO.eq("FAX")));
		telephones = EOPersonneTelephone.fetchAll(getContext(), myQualifier);
		
		if (telephones.count() > 0) {
			return telephones.objectAtIndex(0).noTelephone();
		} else {
			return noString();
		}
	}
	
	
	
}
