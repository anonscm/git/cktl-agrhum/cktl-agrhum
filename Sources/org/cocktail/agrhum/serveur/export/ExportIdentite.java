package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

public class ExportIdentite extends AbstractExportInformation implements IExportInformations{

	
	// Renvoi l'identité : Civilité + Nom + Prénom
	public String get(EOIndividu individu) {
		String identite = individu.toCivilite().cCivilite() + " ";
		identite += individu.getNomAndPrenom();
		return identite;
	}
	
	
	/**
	 * Retourne l'identité de la structure.
	 */
	public String get(EOStructure structure) {
		String identite = structure.getNomCompletAffichage();
		return identite;
	}
}
