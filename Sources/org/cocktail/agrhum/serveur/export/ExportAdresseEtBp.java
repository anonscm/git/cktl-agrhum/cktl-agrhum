package org.cocktail.agrhum.serveur.export;

import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class ExportAdresseEtBp extends AbstractExportInformation implements IExportInformations {
	
	private String typeAdresse;
	
	public ExportAdresseEtBp(String typeAdresse) {
		typeAdresse = typeAdresse;
	}
	
	
	// Renvoi de la partie avec l'adresse  et BP de l'adresse (personnelle ou professionnelle)
	public String get(EOIndividu personne){

		String adresseEtBp;
		EOAdresse adresse = getAdresseTypee(personne, getTypeAdresse());
		if (adresse == null) {
			return noString();
		}
		
		if (adresse.adrAdresse1() != null && !adresse.adrAdresse1().equals("") && !adresse.adrAdresse1().equals(" ")) {
			adresseEtBp = adresse.adrAdresse1();
		} else {
			adresseEtBp = noString();
		}

		if (adresse.adrAdresse2() != null && !adresse.adrAdresse2().equals("") && !adresse.adrAdresse2().equals(" ")) {
			adresseEtBp += " - " + adresse.adrAdresse2();
		}
		if (adresse.adrAdresse3() != null && !adresse.adrAdresse3().equals("") && !adresse.adrAdresse3().equals(" ")) {
			adresseEtBp += " - " + adresse.adrAdresse3();
		}
		if (adresse.adrAdresse4() != null && !adresse.adrAdresse4().equals("") && !adresse.adrAdresse4().equals(" ")) {
			adresseEtBp += " - " + adresse.adrAdresse4();
		}
		if (adresse.adrBp() != null && !adresse.adrBp().equals("") && !adresse.adrBp().equals(" ")) {
			adresseEtBp += " - " + adresse.adrBp();
		}
		
		return adresseEtBp;
	}

	
	public String get(EOStructure structure) {
		return getAdresseEtBp(structure, "PRO");
	}
	
	// Renvoi de la partie avec l'adresse  et BP de l'adresse (personnelle ou professionnelle)
	private String getAdresseEtBp(IPersonne personne, String typeAdresse){

		String adresseEtBp;
		EOAdresse adresse = getAdresseTypee(personne, typeAdresse);
		if (adresse == null) {
			return noString();
		}
		
		if (adresse.adrAdresse1() != null && !adresse.adrAdresse1().equals("") && !adresse.adrAdresse1().equals(" ")) {
			adresseEtBp = adresse.adrAdresse1();
		} else {
			adresseEtBp = noString();
		}

		if (adresse.adrAdresse2() != null && !adresse.adrAdresse2().equals("") && !adresse.adrAdresse2().equals(" ")) {
			adresseEtBp += " - " + adresse.adrAdresse2();
		}
		if (adresse.adrAdresse3() != null && !adresse.adrAdresse3().equals("") && !adresse.adrAdresse3().equals(" ")) {
			adresseEtBp += " - " + adresse.adrAdresse3();
		}
		if (adresse.adrAdresse4() != null && !adresse.adrAdresse4().equals("") && !adresse.adrAdresse4().equals(" ")) {
			adresseEtBp += " - " + adresse.adrAdresse4();
		}
		if (adresse.adrBp() != null && !adresse.adrBp().equals("") && !adresse.adrBp().equals(" ")) {
			adresseEtBp += " - " + adresse.adrBp();
		}
		
		return adresseEtBp;
	}
	
	
	
	//Recherche de l'adresse PRO et PERSO
	@SuppressWarnings("unchecked")
	public EOAdresse getAdresseTypee(IPersonne personne, String typeAdresse) {
		NSArray<EORepartPersonneAdresse> repartPersonneAdresse = null;
		EOAdresse adresse;
		if (typeAdresse == null || typeAdresse.equals("") || typeAdresse.equals(" ")) {
			return  null;
		}
		if (typeAdresse.equals("PRO")) {
			repartPersonneAdresse = (NSArray<EORepartPersonneAdresse>) EORepartPersonneAdresse.adressesValidesDeType(getContext(), personne, "PRO");
		}
		if (typeAdresse.equals("PERSO")) {
			repartPersonneAdresse = (NSArray<EORepartPersonneAdresse>) EORepartPersonneAdresse.adressesPersoValides(getContext(), personne);
		}
		if (typeAdresse.equals("FACT")) {
			repartPersonneAdresse = (NSArray<EORepartPersonneAdresse>) EORepartPersonneAdresse.adressesValidesDeType(getContext(), personne, "FACT");
		}
		
		if (repartPersonneAdresse.count() > 0) {
			adresse = repartPersonneAdresse.objectAtIndex(0).toAdresse();
			return adresse;
			} else {
				return null;
			}
	}
	
	
	public String noString(){
		return "---";
	}


	public String getTypeAdresse() {
		return typeAdresse;
	}

	public void setTypeAdresse(String typeAdresse) {
		this.typeAdresse = typeAdresse;
	}

}
