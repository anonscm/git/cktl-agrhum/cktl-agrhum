package org.cocktail.agrhum.serveur.export;

import static org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl.INFO_ENTREPRISE;
import static org.cocktail.agrhum.serveur.components.controlers.SelectionInfosIndividusCtrl.INFO_FONCTION_INDIVIDU;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ExportInfoEntreprise extends AbstractExportInformation implements IExportInformations{

	
	
	public String get(EOIndividu individu) {
		return getInfosContact(individu);
	}
	
	public String get(EOStructure structure) {
		return noString();
	}
	
	
	public String getInfosContact(EOIndividu individu) {
		String result = noString();
		EOQualifier myQualifier;
		NSMutableDictionary<String, String> myMDico = new NSMutableDictionary<String, String>();
		NSArray<EORepartAssociation> localResult;
		
		myQualifier = EORepartAssociation.PERS_ID.eq(individu.persId()).and(EORepartAssociation.TO_ASSOCIATION.dot(EOAssociation.ASS_CODE).eq("CONTA"));
		
		localResult = EORepartAssociation.fetchAll(getContext(), myQualifier);
		
		if (localResult.count() > 0) {
			EOGenericRecord recordRepartAsso = (EOGenericRecord) localResult.objectAtIndex(0);
			String fonction = ((EORepartAssociation) recordRepartAsso).toAssociation().assLibelle();
			
			String entreprise = "" +  ((EORepartAssociation) recordRepartAsso).toStructure().libelle();
			
			myMDico.setObjectForKey(fonction, INFO_FONCTION_INDIVIDU);
			myMDico.setObjectForKey(entreprise, INFO_ENTREPRISE);

			result =  myMDico.get(INFO_ENTREPRISE);
		}
		
		return result;
	}
}
