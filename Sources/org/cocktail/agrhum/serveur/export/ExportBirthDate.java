package org.cocktail.agrhum.serveur.export;

import java.text.SimpleDateFormat;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.foundation.NSTimestamp;

public class ExportBirthDate extends AbstractExportInformation  implements IExportInformations {
	
	// Renvoi la date de naissance d'un individu
	public String get(
			org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu) {
		if (individu.dNaissance() != null) {
//			SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
			String birthDate = DateCtrl.dateToString(individu.dateNaissance());
			
			return birthDate;
		} else {
			return noString();
		}
	}
	
	public String get(EOStructure structure) {
		return noString();
	}

}
